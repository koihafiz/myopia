# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.5.9-MariaDB)
# Database: myopia
# Generation Time: 2022-07-25 15:32:19 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table committee
# ------------------------------------------------------------

DROP TABLE IF EXISTS `committee`;

CREATE TABLE `committee` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL DEFAULT '',
  `role` varchar(250) DEFAULT NULL,
  `image` varchar(250) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `committee` WRITE;
/*!40000 ALTER TABLE `committee` DISABLE KEYS ;

INSERT INTO `committee` (`id`, `name`, `role`, `image`, `description`, `created_at`, `updated_at`)
VALUES
	(8,'DR. Duratul Ain Hussin','(Optometrist, HKL)','/uploads/committee/1657768177399.png','<p>DR. Duratul currently is a Senior Principal Assistant Director of the Allied Health Sciences Division, the Ministry of Health (MOH) Malaysia. She is an Optometrist by profession, with experience working at various public hospitals throughout Malaysia since graduated from the National University of Malaysia in 2000. Duratul’s PhD research at QUT Australia was on the development and evaluation of primary eye care pathway effectiveness at a health clinic in Malaysia. In addition to her main role within the Ministry, Duratul continues to work on a special project for the expansion of Optometry care services into public community health clinics in Malaysia, which includes strategic partnership with the private sector. She is also coordinating preschool vision screening program and cerebral visual impairment service in the MOH Malaysia since 2017. </p>',1656482253,1657768177),
	(9,'DR. Azlindarita @ Aisyah Mohd Abdullah','(Paediatric ophthalmologist, MSU Medical Centre)','/uploads/committee/1656482409281.png','<p>DR. Aisyah is a General and Paediatric Ophthalmology Consultant in MSU Medical Centre Shah Alam and sub-speciality interest in Paediatric Ophthalmology and Strabismus. She graduated with Bachelor of Medicine (MB CHB BAO) from the National University of Ireland, Galway in Year 2004. She obtained her Masters of Ophthalmology at University Malaya, and sub-speciality training at University Malaya Medical Centre (UMMC). She completed her internship at Ballinasloe Hospital &amp; University College Hospital Galway in Ireland. She is also a member of International Paediatric Ophthalmology and Strabismus Council (IPOSC). Dr Aisyah formally worked as a Medical and General Surgical Officer at Selayang Hospital, and Medical Lecturer and Specialist at UiTM. She has special interest in strabismus in adults and children and myopia control in children in the time of pandemic. She is married to a GP, with 4 kids.</p>',1656482409,1656482409);

!40000 ALTER TABLE `committee` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table education
# ------------------------------------------------------------

DROP TABLE IF EXISTS `education`;

CREATE TABLE `education` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(250) NOT NULL DEFAULT '',
  `short_description` varchar(250) DEFAULT NULL,
  `image` varchar(250) DEFAULT NULL,
  `details` text DEFAULT NULL,
  `author` text DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `education` WRITE;
/*!40000 ALTER TABLE `education` DISABLE KEYS ;

INSERT INTO `education` (`id`, `title`, `short_description`, `image`, `details`, `author`, `created_at`, `updated_at`)
VALUES
	(9,'Myopia or shortsightedness has slowly ','Myopia or shortsightedness has slowly been acknowledged as an increasing global health epidemic; sweeping across the developed as well as the developing countries. Some have likened this to a ticking time bomb.','/uploads/education/1657774861954.png','<p>fdsfsad sdv savads sd fds</p><p><img src=\"http://myopia2.test/uploads/education/1/442c5797dc-petronas.png\" alt=\"442c5797dc-petronas\"></p><p>fasdfdasf</p>','<ol><li>fasdfasdf</li><li>fasdfasd</li><li>fasdfasd</li><li>fasdfas</li></ol>',1656923031,1657774861),
	(10,'dhtfhryj','Currently there are more than 2 and half billion people diagnosed with myopia worldwide. This figure has been estimated to rise to five billion by the year 2050. The hot spots for myopia are seen predominantly','/uploads/education/1657774846335.png','<p>gfgsdfgGsdfgsdfgsdf</p><p>gsdfgds</p><p>gdsfgsdf</p><p><img src=\"http://myopia2.test/uploads/education/1/720ec3d70e-screenshot-2022-05-23-at-91718-am.png\" alt=\"720ec3d70e-screenshot-2022-05-23-at-91718-am\" style=\"width: 169px; height: 67px;\" width=\"169\" height=\"67\"><br>fsdfsadf</p>','<ul><li>fsadfsad</li><li>fsadf</li><li>sadfgsdaf</li><li>sdafgs</li><li>dfgds</li></ul>',1656923815,1657774846);

!40000 ALTER TABLE `education` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table healthcare
# ------------------------------------------------------------

DROP TABLE IF EXISTS `healthcare`;

CREATE TABLE `healthcare` (
  `id` int(12) unsigned NOT NULL AUTO_INCREMENT,
  `category` varchar(250) NOT NULL DEFAULT '',
  `title` varchar(250) NOT NULL DEFAULT '',
  `description` text DEFAULT NULL,
  `image` varchar(250) DEFAULT NULL,
  `details` text DEFAULT NULL,
  `created_at` int(12) DEFAULT NULL,
  `updated_at` int(12) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `healthcare` WRITE;
/*!40000 ALTER TABLE `healthcare` DISABLE KEYS ;

INSERT INTO `healthcare` (`id`, `category`, `title`, `description`, `image`, `details`, `created_at`, `updated_at`)
VALUES
	(5,'Refractive Error In Children','WSPOS Myopia consensus statement','rqwerqwer rqwerqwer rqwerqwer rqwerqwer rqwerqwer rqwerqwer rqwerqwer rqwerqwer rqwerqwer','/uploads/health/1657775511129.png','<p>rqwerqwe</p><p>rqwerqwe</p><p>rtweq</p><p><img src=\"http://myopia2.test/uploads/education/1/6b6ef82516-petronas.png\">rqwer</p><p>rqwetwqetwetr</p>',1657122737,1657775511),
	(7,'Myopia Management Guidelines','Management Guidelines','We developed this world first, clinically focused infographic for the American Academy of Optometry (AAO) meeting in Orlando, Florida in late October 2019. In collaboration with the Centre for Ocular Research and Education (CORE), University of Waterloo, Canada.','/uploads/health/1657775527415.png','<p>Building on evidence of the benefit of outdoor activity for prevention and control of myopia, <strong>Wu et al. </strong>imple­mented a program to encourage young Taiwanese schoolchildren to spend more time outside. After 1 year, those students who had been encouraged to spend at least 11 hours per week outdoors, with exposure to light intensity of at least 1,000 lux, had signifi­cantly less myopic shift and axial elongation than did those in the control group.\r\n</p><p>The study included 693 first graders at 16 schools. The intervention group (n = 267) participated in school-oriented outdoor activities, including fresh-air recess and summer learning assignments, and they were encouraged to spend at least 11 hours per week outside. The control group (n = 426) did not receive these interventions but spent some time outside. Both groups had outdoor exercise initiatives. All participants wore a light meter record­er and, with help from their parents, completed weekly activity diaries and questionnaires. Time outdoors was defined as the period during which light intensity was at least 1,000 lux according to the light meter. Outcomes of interest were changes in spherical equivalent and axial length from base­line to 1 year, as well as intensity and duration of exposure to outdoor light.\r\n</p><p>The researchers found that more students in the intervention group (50% vs. 23% of controls) spent more than 11 hours per week outdoors. Stu­dents who spent at least 200 minutes per week outside during school hours were found to have significantly less myopic shift with lux readings as follows: ≥1,000 lux, 0.14 D; ≥3,000 lux, 0.16 D. The interven­tion group had significantly less myopic shift than the control group (0.35 D vs. 0.47 D) and axial elon­gation (0.28 mm vs. 0.33 mm). The risk of rapid myopia progression was 54% lower in the intervention group (odds ratio, 0.46; p = .003). The protective effects against myopia were seen among myopic and nonmyopic children in the intervention group.\r\n</p><p>The authors concluded that exposure to strong sunlight may not be required for prevention of myopia. Longer periods of relatively low outdoor light intensity, as in the shade of trees, may be sufficient for the protective effect. Larger studies of longer duration are warranted. <em>(Also see related commentary by Ian G. Mor­gan, BSc, PhD, in the same issue.)</em>\r\n</p>',1657244666,1657775527),
	(8,'Refractive Error In Children','Myopia consensus problem','rqwerqwer rqwerqwer rqwerqwer rqwerqwer rqwerqwer rqwerqwer rqwerqwer rqwerqwer rqwerqwer','/uploads/health/1657122737315.png','<p>rqwerqwe</p><p>rqwerqwe</p><p>rtweq</p><p><img src=\"http://myopia2.test/uploads/education/1/6b6ef82516-petronas.png\" style=\"\" rel=\"\">rqwer</p><p>rqwetwqetwetr</p>',1657122737,1657252152),
	(9,'Myopia Prevalence','Myopia Rate Increasing in Taiwanese Children','Analyzed population-based data to better understand myopia rates among schoolchildren in ','/uploads/health/1657264526190.png','<h3>ournal Highlights</h3>\r\n<p><em>Ophthalmology</em>, February 2021\r\n</p>\r\n<p><a href=\"https://www.aao.org/Assets/41ec2c83-fed9-48cd-bcd8-53d366c4f171/637464007705100000/february-2021-journal-highlights-pdf\" data-feathr-click-track=\"true\">Download PDF<br><br></a>\r\n</p>\r\n<p><strong>Tsai et al. </strong>analyzed population-based data to better understand myopia rates among schoolchildren in Taiwan. They found that myopia prevalence has risen rapidly in this group since 1983. Major risk factors are older age and heavy involvement in near-work activities.\r\n</p>\r\n<p>For this study, the authors looked at findings of eight population-based surveys on myopia that were conducted from 1983 through 2017 in children be­tween the ages of 3 and 18. The number of participants per survey ranged from 5,019 to 11,656. Comprehensive oph­thalmic evaluations were performed, and tropicamide 0.5% was applied to obtain cycloplegic refractive status. My­opia and high myopia were defined as spherical equivalents at or below –0.25 D and –6.0 D, respectively. Multivariate analyses were used to determine risk factors.\r\n</p>\r\n<p>Results showed that myopia prevalence climbed steadily in all age groups during the study period. For example, the weighted prevalence rose from 5.37% to 25.41% among 7-year-olds and from 30.66% to 76.67% for 12-year-olds during this period (p = .001 for both trends). The prevalence of high myopia grew from 1.39% to 4.26% among 12-year-olds (p = .008) and from 4.37% to 15.36% among 15-year-olds (p = .039). In the 2005 and 2016 population surveys, children who spent &lt;180 minutes/day on near work were less likely to develop myopia. The 2016 data showed that spending more than one hour a day on electronic devices increased the likelihood of myopia and high myopia (OR, 2.43 and 2.31).\r\n</p>\r\n<p>The authors noted that their defini­tion of myopia and choice of cyclople­gic agent may have contributed to the relatively high prevalence of myopia in their study, especially in the assessment of the young­est children. Other experts reported that up to four drops of 1% cyclopentolate may be needed to accurately measure refractive error in some young children. These issues require explo­ration in carefully designed multicenter studies, said the authors. They recommend that eye care specialists and policymakers be cognizant of the increase in near-work time that stems from greater use of electronic devices.\r\n</p>\r\n<p><a href=\"https://www.aaojournal.org/article/S0161-6420(20)30679-5/fulltext\" data-feathr-click-track=\"true\">The original article can be found here.</a>\r\n</p>',1657264526,1657264526),
	(10,'Myopia Prevalence','Epidemiology of Myopia','There is a high prevalence of myopia, 80% to 90%, in young adults in East Asia; myopia has become the leading..','/uploads/health/1657264671530.png','<h2><span class=\"fl-heading-text\">Updated versions and eight language translations launched November 2020</span></h2><p>Our hugely popular infographics, parent brochures and personalized inserts have all been updated to be language based rather than country based. <strong>Each download includes three versions</strong> to reflect different regions utilizing the same language: A4 size, A4 with atropine removed, and Letter size.</p><p><span class=\"d2edcug0 hpfvmrgz qv66sw1b c1et5uql rrkovp55 jq4qci2q a3bd9o3v knj5qynh oo9gr5id\">Our current translations of English, Spanish, French and Romanian are freshly updated to reflect the latest scientific research and clinical messaging on myopia control efficacy. We\'ve added the DIMS technology spectacle lens, being commercialized by Hoya as MiyoSmart; and delineated centre-distance multifocal contact lens efficacy from the CooperVision MiSight lens, based on the recently published <a class=\"oajrlxb2 g5ia77u1 qu0x051f esr5mh6w e9989ue4 r7d6kgcz rq0escxv nhd2j8a9 nc684nl6 p7hjln8o kvgmc6g5 cxmmr5t8 oygrvhab hcukyx3x jb3vyjys rz4wbd8a qt6c0cv9 a8nywdso i1ao9s8h esuyzwwr f1sip0of lzcic4wl py34i1dx gpro0wi8\" tabindex=\"0\" role=\"link\" href=\"https://l.facebook.com/l.php?u=https%3A%2F%2Fmyopiaprofile.com%2Fblink-study-results-multifocal-contact-lens%2F%3Fmc_cid%3D2f2d72eec3%26mc_eid%3D[UNIQID]%26fbclid%3DIwAR0gV5DSCJw1gvYxb6lLOh283lAzHz9h4L3N1Szb_q9M4iVImI-JQLohIcI&amp;h=AT24BRcoU-vOC5uVVVH6ECSdvC5vXJiyw0lbHQbvPSY_HZkw017EUNeJCtXCSNKSdH17Q3pIW89RnxvhFIP6ptsot4VAWr-_MMIdYNMWzHbnf4Cc6aFhOV7RwrM2tf5TsA&amp;__tn__=-UK-R&amp;c[0]=AT3dqt9eWZd8_LRmE7ft3IkXuJLt7ribNxNA8PTdBN2y9iLFG3y2stYy-HhcEqHSfgv9XJe2na-M0GyoXcCAIpX6qrHRXdPRGMw0mcvggfTY8OTZmjq3QOtmRb8VDkjjGaH3asvFfhVKKdBsUmTJKC76zl4vXMgQXzsOZk_XzbTj01677HSvExoQ8Z-k\" target=\"_blank\">BLINK Study</a>.</span></p><p><span class=\"d2edcug0 hpfvmrgz qv66sw1b c1et5uql rrkovp55 jq4qci2q a3bd9o3v knj5qynh oo9gr5id\">- We\'ve also changed the imagery to children on bicycles, with the \'single vision wearing\' bicycle child without a helmet to protect him from rushing towards high myopia. You\'ll notice the helmets become more robust as the \'protection\' offered by myopia control options increases in efficacy.</span></p><p>- We added four new translations - Hebrew, Arabic, Simplified Chinese and Traditional Chinese.</p><p>To put these into practice, you can:</p><ul><li>Download them <a href=\"https://myopiaprofile.com/store/\">HERE</a></li><li>Watch our explanatory YouTube video <a href=\"https://www.youtube.com/watch?v=TEf1CfhYBWo&amp;t=9s\" target=\"_blank\">HERE</a></li><li>Link and share our explanatory video for parents, to reinforce your explanations, from our MyKidsVision.org How-To Video Guides <a href=\"https://www.mykidsvision.org/en-us/Home/HowToGuides\" target=\"_blank\">HERE.</a></li></ul><p>Further reading and references are provided below. You can also read the history of the Managing Myopia Guidelines Infographics from their first launch in late 2019.</p>',1657264671,1657264671),
	(11,'Signs Of Vision Problem In Kids','ffadfasfasd fasdfasdf','fsadf fasdfa sdfasd fasd','','<p>fadsfas fdsaf asdfasdf asdfasdf</p>',1657593414,1657593414),
	(12,'Treatment Of Myopia Control In Children','arqewrqw fsdf fsdfsdaf fasdfa fsdafgsdagf fsadf','qwerqewr qwerqewr qwerqewr qwerqewr qwerqewr qwerqewr qwerqewr qwerqewr qwerqewr qwerqewr qwerqewr qwerqewr qwerqewr qwerqewr qwerqewr qwerqewr qwerqewr qwerqewr qwerqewr qwerqewr qwerqewr qwerqewr qwerqewr qwerqewr qwerqewr qwerqewr qwerqewr qwerqewr qwerqewr qwerqewr qwerqewr qwerqewr qwerqewr qwerqewr qwerqewr qwerqewr qwerqewr qwerqewr qwerqewr qwerqewr qwerqewr qwerqewr qwerqewr qwerqewr','/uploads/health/1657594463248.png','<p>wqerqwerqwer</p>',1657594463,1657594463),
	(13,'Covid-19 Lockdown Changed Children’S Eyes','covid','aerfweqvwev cwevrewf','','<p>wqefqwef</p>',1657594976,1657594976);

!40000 ALTER TABLE `healthcare` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table migration
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migration`;

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `migration` WRITE;
/*!40000 ALTER TABLE `migration` DISABLE KEYS */;

INSERT INTO `migration` (`version`, `apply_time`)
VALUES
	('m000000_000000_base',1654832849),
	('m130524_201442_init',1654832853),
	('m190124_110200_add_verification_token_column_to_user_table',1654832854);

/*!40000 ALTER TABLE `migration` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table treatment
# ------------------------------------------------------------

DROP TABLE IF EXISTS `treatment`;

CREATE TABLE `treatment` (
  `id` int(12) unsigned NOT NULL AUTO_INCREMENT,
  `category` varchar(250) NOT NULL DEFAULT '',
  `title` varchar(250) NOT NULL DEFAULT '',
  `description` text DEFAULT NULL,
  `image` varchar(250) DEFAULT NULL,
  `details` text DEFAULT NULL,
  `created_at` int(12) DEFAULT NULL,
  `updated_at` int(12) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `treatment` WRITE;
/*!40000 ALTER TABLE `treatment` DISABLE KEYS *;

INSERT INTO `treatment` (`id`, `category`, `title`, `description`, `image`, `details`, `created_at`, `updated_at`)
VALUES
	(14,'Low Dose Atropine','fasdfasfda dc dsf dsafasdgf gsfg xxx fsdfsdf dsg','fasdfas sdvevewsd x ewvwerfw sfgdsvdsfvdsf vsdfgsdfgv sdfvds vdsfgvsdfgvsfgfs gsfgfsxzvgdsf ','/uploads/treat/1657641825778.png','<p>qwerqwav sdvwevcwe</p><p>wqefqwec</p><p>cvwdcw\'</p><p><img src=\"http://myopia2.test/uploads/education/1/542ec7a0a4-screenshot-2021-06-18-at-95434-pm.png\" style=\"width: 367px; height: 146px;\" width=\"367\" height=\"146\">fwefwqce nffvsdv sgewrgs vcasdfvqwe</p><p>feafqwvd wevr</p>',1657604954,1657641825),
	(15,'Spectacle','dawrqwer sfgrsdfgv dgdfgdf gsdfgsd','rwqerqwerqwerqwer as sdvevewsd x ewvwerfw sfgdsvdsfvdsf vsdfgsdfgv sdfvds vdsfgvsdfgvsfgfs gsfgfsxzvgdsf ','/uploads/treat/1657675801868.png','<p>sdafsd fweterwtewrtre</p><p> ert</p><p>t ertwer</p><p> tewrtewrt</p>',1657675801,1657675801),
	(16,'Contact Lenses','dasdaw ETY ERTYtr  fsdfgf sfgsfgfdsg gsd',' WQERRT WET EWRTEYERter tretgert as sdvevewsd x ewvwerfw sfgdsvdsfvdsf vsdfgsdfgv sdfvds vdsfgvsdfgvsfgfs gsfgfsxzvgdsf ','/uploads/treat/1657675841586.png','<p>r wert e gt gert re tr</p><p> ter twegrgre</p><p> gstg rtetgt</p>',1657675841,1657675841),
	(17,'Contact Lenses','trete yryuyrh yrtyh5ehrthy hdyfhyh ryhrtyh rhdrhdfyh hdrth dr htyh gdgbfh f','sdfgds gdhdfhb dfghdfg hdfghdfg hdxghdf hdfghdfg','/uploads/treat/1657678338123.png','<p>rfsadfsaf fsdfaf<sup>gh. gdfhgdf gdfxgd</sup></p><p><sup>ewrqwrqwer</sup></p><p><sup><br></sup></p>',1657678338,1657678338),
	(18,'Outdoor Activities','rqwef edf vewrrv43etvd verbv43tvd df. bgre berf vfe erg dvdfv erg 4te rfd ','wr trtgewsdfergtb ebrgvbert ver gertgverv ergbertbv wvbersv bertg wr trtgewsdfergtb ebrgvbert ver gertgverv ergbertbv wvbersv bertg wr trtgewsdfergtb ebrgvbert ver gertgverv ergbertbv wvbersv bertg wr trtgewsdfergtb ebrgvbert ver gertgverv ergbertbv wvbersv bertg','/uploads/treat/1657684996357.png','<p>aefqw</p><p>fwergwertv</p><p>ertwv</p><p>wert</p><p><img src=\"http://myopia2.test/uploads/education/1/dfcd4db4b0-screenshot-2021-06-18-at-95434-pm.png\" style=\"width: 827px; height: 327px;\" width=\"827\" height=\"327\"></p>',1657684996,1657684996);

!40000 ALTER TABLE `treatment` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT 10,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `verification_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `password_reset_token` (`password_reset_token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS *;

INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`, `verification_token`)
VALUES
	(1,'hafiz','DMYnik3SaDVFwW57cGazA-zh6MfjNFKH','$2y$13$bhFMSDNAKTf9uoUIaBZ7n..FXTR7C0zIy1.Dssi4mWw/XVJhKuU3m',NULL,'koihafiz@gmail.com',10,1656091965,1656091965,'tFf0srVrdQ9wlqniaIi_yENM2CYrgF_0_1656091965');

*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table video
# ------------------------------------------------------------

DROP TABLE IF EXISTS `video`;

CREATE TABLE `video` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(250) DEFAULT '',
  `iframe` text DEFAULT NULL,
  `src` text DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `video` WRITE;
/*!40000 ALTER TABLE `video` DISABLE KEYS *;

INSERT INTO `video` (`id`, `title`, `iframe`, `src`, `created_at`, `updated_at`)
VALUES
	(1,'1231df','<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/1lbriMCMEC8\" title=\"YouTube video player\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>','',1656946640,1657769117),
	(3,'adsfadfs','<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/XNzycjDLbTA\" title=\"YouTube video player\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>',NULL,1656965042,1656965042),
	(4,'dsadeqeq','<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/ldfJGTC0ZQ0\" title=\"YouTube video player\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>',NULL,1656965151,1656965151);

*!40000 ALTER TABLE `video` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
