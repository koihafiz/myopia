<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
//        'css/site.css',
//        'plugins/bootstrap/css/bootstrap.min.css',
          'plugins/icofont/icofont.min.css',
          'plugins/slick-carousel/slick/slick.css',
          'plugins/fontawesome/font-awesome.min.css',
          'plugins/animate/animate.css',
          'plugins/slick-carousel/slick/slick-theme.css',
          'css/style.css',
    ];
    public $js = [
//        'plugins/jquery/jquery.js',
        'plugins/bootstrap/js/popper.js',
        'plugins/bootstrap/js/bootstrap.min.js',
        'plugins/counterup/jquery.easing.js',
        'plugins/slick-carousel/slick/slick.min.js',
        'plugins/counterup/jquery.waypoints.min.js',

        'plugins/shuffle/shuffle.min.js',
        'plugins/counterup/jquery.counterup.min.js',

        'js/script.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap4\BootstrapAsset',
    ];
}
