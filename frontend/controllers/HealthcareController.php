<?php

namespace frontend\controllers;

use common\models\Healthcare;
use common\models\HealthcareSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use Yii;

/**
 * HealthcareController implements the CRUD actions for Healthcare model.
 */
class HealthcareController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'access' => [
                    'class' => AccessControl::className(),
                    'rules' => [
                        [
                            'allow' => true,
                            'roles' => ['@'],
                        ],
                    ],
                ],
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Healthcare models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new HealthcareSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Healthcare model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Healthcare model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Healthcare();

        if ($this->request->isPost) {
            if ($model->load($this->request->post())) {

//                create /uploads/committee folder if not exist yet
//                ===================================================================
                $project_dir = Yii::getAlias('@myroot/uploads/health');

                if (!file_exists($project_dir)) {
                    if (!mkdir($project_dir, 0777, true) && !is_dir($project_dir)) {
                        throw new \RuntimeException(sprintf('Directory "%s" was not created', $project_dir));
                    }
                }

//                ==================================================================

                $model->img = UploadedFile::getInstance($model, 'img');

                if($model->img)
                    $model->image = '/uploads/health/' . (string)time() . rand(100,999) . '.' . $model->img->extension;
                else
                    $model->image = '';

                if($model->save())
                {
                    if($model->img)
                        $model->img->saveAs(Yii::getAlias('@myroot') . $model->image);

                    return $this->redirect(['view', 'id' => $model->id]);
                }
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Healthcare model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate1($id)
    {
        $model = $this->findModel($id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($this->request->isPost && $model->load($this->request->post())) {

//                create /uploads/committee folder if not exist yet
//                ===================================================================
            $project_dir = Yii::getAlias('@myroot/uploads/health');

            if (!file_exists($project_dir)) {
                if (!mkdir($project_dir, 0777, true) && !is_dir($project_dir)) {
                    throw new \RuntimeException(sprintf('Directory "%s" was not created', $project_dir));
                }
            }

//                ==================================================================
            $model->img = UploadedFile::getInstance($model, 'img');

            if($model->img){
                $directory = Yii::getAlias('@myroot');

                if (is_file($directory . $model->image)) {
                    unlink($directory . $model->image);
                }

                $model->image = '/uploads/health/' . (string)time() . rand(100,999) . '.' . $model->img->extension;

            }
            else
                $model->image = '';

//            return $this->redirect(['view', 'id' => $model->id]);
            if($model->save())
            {
                if($model->img)
                    $model->img->saveAs(Yii::getAlias('@myroot') . $model->image);

                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Healthcare model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $directory = Yii::getAlias('@myroot');

        if (is_file($directory . $model->image)) {
            unlink($directory . $model->image);
        }

        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Healthcare model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return Healthcare the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Healthcare::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
