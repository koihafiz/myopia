<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
//use dosamigos\tinymce\TinyMce;

/* @var $this yii\web\View */
/* @var $model common\models\Committee */
/* @var $form yii\widgets\ActiveForm */

?>
<style>
    .custom-file-input.selected:lang(en)::after {
        content: "" !important;
    }
    .custom-file {
        overflow: hidden;
    }
    .custom-file-input {
        white-space: nowrap;
    }
</style>
<div class="committee-form mt-5">

    <?php $form = ActiveForm::begin([
        'options' => [
            'enctype'=>'multipart/form-data',
            'class' => ''
        ]]); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'role')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group field-committee-img">
                <label class="control-label" for="committee-img">Image</label>
                <div class="custom-file">
                    <input type="hidden" name="Committee[img]" value="">
                    <input type="file" class="form-control custom-file-input" id="committee-img" onchange="putImage()" accept=".png,.jpg,.jpeg" name="Committee[img]" aria-describedby="committee-img">
                    <label class="custom-file-label" for="committee-img">Select file</label>
                </div>
                <div class="help-block"></div>
            </div>

        </div>
        <div class="col-md-6">
            <img id="selected_img" src="<?=$model->image ?>" width="50%" alt="">
        </div>
    </div>





    <?php //= $form->field($model, 'img')->fileInput() ?>

    <?php //= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <script>
        function pickFile(cb, value, meta) {
            var input = document.createElement('input');
            input.setAttribute('type', 'file');
            input.setAttribute('accept', 'image/*');

            /*
              Note: In modern browsers input[type="file"] is functional without
              even adding it to the DOM, but that might not be the case in some older
              or quirky browsers like IE, so you might want to add it to the DOM
              just in case, and visually hide it. And do not forget do remove it
              once you do not need it anymore.
            */

            input.onchange = function() {
                //alert('File Input Changed');
                console.log( this.files[0] );

                var file = this.files[0];

                var reader = new FileReader();
                reader.readAsDataURL(file);
                reader.onload = function () {
                    // Note: Now we need to register the blob in TinyMCEs image blob
                    // registry. In the next release this part hopefully won't be
                    // necessary, as we are looking to handle it internally.

                    //Remove the first period and any thing after it
                    var rm_ext_regex = /(\.[^.]+)+/;
                    var fname = file.name;
                    fname = fname.replace( rm_ext_regex, '');

                    //Make sure filename is benign
                    var fname_regex = /^([A-Za-z0-9])+([-_])*([A-Za-z0-9-_]*)$/;
                    if( true ) {
                        var id = fname + '-' + (new Date()).getTime(); //'blobid' + (new Date()).getTime();
                        var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
                        var blobInfo = blobCache.create(id, file, reader.result);
                        blobCache.add(blobInfo);

                        // call the callback and populate the Title field with the file name
                        callback(blobInfo.blobUri(), { title: file.name });
                    }
                    else {
                        alert( 'Invalid file name' );
                    }
                };
                //To get get rid of file picker input
                // this.parentNode.removeChild(this);
            };

            input.click();
        }

        function image_upload_handler(blobInfo, success, failure, progress) {
            var xhr, formData;

            xhr = new XMLHttpRequest();
            xhr.withCredentials = false;
            xhr.open('POST', '<?=Url::to('/site/image-upload')?>');

            xhr.upload.onprogress = function (e) {
                progress(e.loaded / e.total * 100);
            };

            xhr.onload = function() {
                var json;

                if (xhr.status === 403) {
                    failure('HTTP Error: ' + xhr.status, { remove: true });
                    return;
                }

                if (xhr.status < 200 || xhr.status >= 300) {
                    failure('HTTP Error: ' + xhr.status);
                    return;
                }

                json = JSON.parse(xhr.responseText);

                if (!json || typeof json.location != 'string') {
                    failure('Invalid JSON: ' + xhr.responseText);
                    return;
                }

                success(json.location);
            };

            xhr.onerror = function () {
                failure('Image upload failed due to a XHR Transport error. Code: ' + xhr.status);
            };

            formData = new FormData();

            formData.append('file', blobInfo.blob(), blobInfo.filename());
            console.log(blobInfo.blob());

            xhr.send(formData);
        };

        // tinymce.init({
        //     selector: 'textarea',  // change this value according to your HTML
        //     images_upload_handler: example_image_upload_handler
        // });


    </script>

    <?= $form->field($model, 'description')->widget(TinyMce::className(), [
        'options' => ['rows' => 4],
//        'language' => 'eng',
        'clientOptions' => [
//            'plugins' => [
//                "advlist autolink lists link charmap preview anchor",
//                "searchreplace visualblocks code fullscreen",
//                "insertdatetime media table contextmenu paste image"
//            ],
//        'image_title' => true,
        'automatic_uploads' => true,
            'file_picker_callback' => new \yii\web\JsExpression('pickFile'),
//            'file_browser_callback' => 'pickFile',
            'plugins' => [
                  'a11ychecker','advlist','advcode','advtable','autolink','checklist','export',
                  'lists','link','image','charmap','anchor','searchreplace','visualblocks',
                  'powerpaste','fullscreen','formatpainter','insertdatetime','media','table','help'
        ],
//            'images_upload_url' => \yii\helpers\Url::to('/site/image-upload'),
//            'images_upload_handler' => new \yii\web\JsExpression('image_upload_handler'),
            'toolbar' => "undo redo | styleselect | bold italic underline| alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
        ]
    ]);?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
