<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Committee */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Committees'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<style>
    .img_style {width: 253px}

</style>
<div class="container">

    <h3><?= Html::encode($this->title) ?></h3>

    <p class="mb-5">
        <?= Html::a(Yii::t('app', 'Main List'), ['index'], ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
//            'id',
            'name',
            'role',
//            'image',
            [
                'attribute' => 'image',
//                'contentOptions' => ['class' => 'text-black-600 align-items-center'],
//                'headerOptions' => ['class' => 'fw-bold text-primary'],
                'format' =>  ['html'],
                'value' => function($model) {
                    return Html::img(Yii::$app->params['custom_url'] . ltrim($model->image,'/'), ['class' => 'img_style']);
                },
                'filter' => false,
            ],
//            'description:ntext',
            [
                'attribute' => 'description',
                'contentOptions' => ['class' => 'text-black-600 align-items-center'],
                'headerOptions' => ['class' => 'fw-bold text-primary'],
                'format' =>  ['html'],
                'value' => function($model) {
                    return $model->description;
                },
                'filter' => true,
            ],
            'created_at:datetime',
            'updated_at:datetime',
        ],
    ]) ?>

</div>
