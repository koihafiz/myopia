<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
//use dosamigos\tinymce\TinyMce;
use yii\redactor\widgets\Redactor;

/* @var $this yii\web\View */
/* @var $model common\models\Committee */
/* @var $form yii\widgets\ActiveForm */

?>
<style>
    .custom-file-input.selected:lang(en)::after {
        content: "" !important;
    }
    .custom-file {
        overflow: hidden;
    }
    .custom-file-input {
        white-space: nowrap;
    }
    .redactor-editor {min-height: 250px !important;}
</style>
<div class="committee-form mt-5">

    <?php $form = ActiveForm::begin([
        'options' => [
            'enctype'=>'multipart/form-data',
            'class' => ''
        ]]); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'role')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group field-committee-img">
                <label class="control-label" for="committee-img">Image</label>
                <div class="custom-file">
                    <input type="hidden" name="Committee[img]" value="">
                    <input type="file" class="form-control custom-file-input" id="committee-img" onchange="putImage()" accept=".png,.jpg,.jpeg" name="Committee[img]" aria-describedby="committee-img">
                    <label class="custom-file-label" for="committee-img">Select file</label>
                </div>
                <div class="help-block"></div>
            </div>

        </div>
        <div class="col-md-6">
            <img id="selected_img" src="<?=Yii::$app->params['custom_url'] . ltrim($model->image,'/') ?>" width="50%" alt="">
        </div>
    </div>





    <?php //= $form->field($model, 'img')->fileInput() ?>

    <?php //= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <script>


    </script>

    <h6 class="mt-5">For Detail Page</h6>
    <hr class="footer-btm py-2">

    <?= $form->field($model, 'description')->widget(Redactor::className(),
        [
            'value'=>0,
            'clientOptions'=>
                [
                    'buttons'=>
                        [
                            'format', 'bold', 'italic', 'underline','font', 'lists', 'image', 'horizontalrule','indent','outdent','alignment','orderedlist','unorderedlist'
                        ],
                    'plugins' => ['imagemanager']
                ]
        ]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
