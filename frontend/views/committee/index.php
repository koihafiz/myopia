<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\CommitteeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Committee List');
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    a[title=View] {color: #007bff;}
    a[title=Update] {color: #28a745;}
    a[title=Delete] {color: #dc3545;}
    .img_style {width: 113px}

</style>

<div class="container">

    <h3><?= Html::encode($this->title) ?></h3>

    <p>
        <?= Html::a(Yii::t('app', 'Create Committee'), ['create'], ['class' => 'btn btn-success mb-5']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            'name',
            'role',
//            'image',
            [
                'attribute' => 'image',
//                'contentOptions' => ['class' => 'text-black-600 align-items-center'],
//                'headerOptions' => ['class' => 'fw-bold text-primary'],
                'format' =>  ['html'],
                'value' => function($model) {
                    return Html::img(Yii::$app->params['custom_url'] . ltrim($model->image,'/'), ['class' => 'img_style']);
                },
                'filter' => false,
            ],
//            'description:ntext',
            [
                'attribute' => 'description',
                'contentOptions' => ['class' => 'text-black-600 align-items-center'],
                'headerOptions' => ['class' => 'fw-bold text-primary'],
                'format' =>  ['html'],
                'value' => function($model) {
                    return $model->description;
                },
                'filter' => false,
            ],
            //'created_at',
            //'updated_at',
//            [
//                'class' => ActionColumn::className(),
//                'urlCreator' => function ($action, Committee $model, $key, $index, $column) {
//                    return Url::toRoute([$action, 'id' => $model->id]);
//                 }
//            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => '<a href="#">Action</a>',
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
