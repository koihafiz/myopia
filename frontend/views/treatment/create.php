<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Treatment */

$this->title = Yii::t('app', 'Create Treatment');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Treatments'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="treatment-create container">

    <h3><?= Html::encode($this->title) ?></h3>
    <?= Html::a(Yii::t('app', 'Main List'), ['index'], ['class' => 'btn btn-success mb-5']) ?>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
<?php $this->beginBlock('JsBlock') ?>
    <script type="text/javascript">
        $(document).ready(function(){

            document.querySelector('.custom-file-input').addEventListener('change', function (e) {
                var name = document.getElementById("treatment-img").files[0].name;
                var nextSibling = e.target.nextElementSibling
                nextSibling.innerText = name
            });

            $('#treatment-category').on('change', function() {
                let value = this.value;

//              init!
                $('#img_row').show();

            });



        });


        function showImage(src, target) {
            var fr = new FileReader();
            fr.onload = function(){
                target.src = fr.result;
            }
            fr.readAsDataURL(src.files[0]);
        }

        function putImage() {
            var src = document.getElementById("treatment-img");
            var target = document.getElementById("selected_img");
            showImage(src, target);
        }
    </script>
<?php $this->endBlock()?>