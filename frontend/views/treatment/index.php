<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\TreatmentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Treatments');
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    a[title=View] {color: #007bff;}
    a[title=Update] {color: #28a745;}
    a[title=Delete] {color: #dc3545;}
    .img_style {width: 113px}

</style>
<div class="container">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Treatment'), ['create'], ['class' => 'btn btn-success mb-5']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            'category',
            'title',
            'description:ntext',
//            'image',
            [
                'attribute' => 'image',
//                'contentOptions' => ['class' => 'text-black-600 align-items-center'],
//                'headerOptions' => ['class' => 'fw-bold text-primary'],
                'format' =>  ['html'],
                'value' => function($model) {
                    return Html::img(Yii::$app->params['custom_url'] . ltrim($model->image,'/'), ['class' => 'img_style']);
                },
                'filter' => false,
            ],
            //'details:ntext',
            //'created_at',
            //'updated_at',
            'created_at:datetime',
            //'updated_at',
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => '<a href="#">Action</a>',
            ],
        ],
    ]); ?>


</div>
