<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Treatment */
/* @var $form yii\widgets\ActiveForm */

$listData = [
    'Myopia Control In Children' => 'Myopia Control In Children',
    'Low Dose Atropine' => 'Low Dose Atropine',
    'Spectacle' => 'Spectacle',
    'Contact Lenses' => 'Contact Lenses',
    'Outdoor Activities' => 'Outdoor Activities',
];
?>
<style>
    .help-block {color: red}
    .redactor-editor {min-height: 250px !important;}
</style>
<div class="treatment-form">

    <?php $form = ActiveForm::begin([
        'options' => [
            'enctype'=>'multipart/form-data',
            'class' => ''
        ]]); ?>

    <?= $form->field($model, 'category')->dropDownList($listData, ['prompt'=>'Choose Category...']) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <div class="row" id="img_row">
        <div class="col-md-6">
            <div class="form-group field-treatment-img">
                <label class="control-label" for="treatment-img">Image</label>
                <div class="custom-file">
                    <input type="hidden" name="Treatment[img]" value="">
                    <input type="file" class="form-control custom-file-input" id="treatment-img" onchange="putImage()" accept=".png,.jpg,.jpeg" name="Treatment[img]" aria-describedby="treatment-img">
                    <label class="custom-file-label" for="treatment-img">Select file</label>
                </div>
                <div class="help-block"></div>
            </div>

        </div>
        <div class="col-md-6">
            <img id="selected_img" src="<?=Yii::$app->params['custom_url'] . ltrim($model->image,'/') ?>" width="50%" alt="">
        </div>
    </div>


    <?php //= $form->field($model, 'image')->textInput(['maxlength' => true]) ?>

    <h6 class="mt-5">For Detail Page</h6>
    <hr class="footer-btm py-2">

    <?= $form->field($model, 'details')->widget(\yii\redactor\widgets\Redactor::className(),
        [
            'value'=>0,
            'clientOptions'=>
                [
                    'buttons'=>
                        [
                            'formatting', 'html', 'bold', 'italic', 'underline','font', 'lists', 'image', 'horizontalrule','indent','outdent','alignment','orderedlist','unorderedlist'
                        ],
                    'plugins' => ['imagemanager','fontsize'],
//                    'buttons' => [
//                        'formatting', '|', 'bold', 'italic', 'deleted', '|',
//                        'unorderedlist', 'orderedlist', 'outdent', 'indent', '|',
//                        'image', 'table', 'link', '|',
//                        'alignment', '|', 'horizontalrule',
//                        '|', 'underline', '|', 'alignleft', 'aligncenter', 'alignright', 'justify'
//                    ],
                ]
        ]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
