<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Healthcare */

$this->title = Yii::t('app', 'Create Healthcare');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Healthcares'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">

    <h3><?= Html::encode($this->title) ?></h3>
    <?= Html::a(Yii::t('app', 'Main List'), ['index'], ['class' => 'btn btn-success mb-5']) ?>


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
<?php $this->beginBlock('JsBlock') ?>
    <script type="text/javascript">
        $(document).ready(function(){

            document.querySelector('.custom-file-input').addEventListener('change', function (e) {
                var name = document.getElementById("healthcare-img").files[0].name;
                var nextSibling = e.target.nextElementSibling
                nextSibling.innerText = name
            });

            $('#healthcare-category').on('change', function() {
                let value = this.value;

//              init!
                $('#img_row').show();

                if(value == 'Myopia Management Guidelines' || value == 'Signs Of Vision Problem In Kids' || value == 'Covid-19 Lockdown Changed Children’S Eyes')
                    $('#img_row').hide();
            });



        });


        function showImage(src, target) {
            var fr = new FileReader();
            fr.onload = function(){
                target.src = fr.result;
            }
            fr.readAsDataURL(src.files[0]);
        }

        function putImage() {
            var src = document.getElementById("healthcare-img");
            var target = document.getElementById("selected_img");
            showImage(src, target);
        }
    </script>
<?php $this->endBlock()?>