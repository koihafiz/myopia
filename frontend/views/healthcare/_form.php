<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Healthcare */
/* @var $form yii\widgets\ActiveForm */

$listData = [
    'Myopia Management Guidelines' => 'Myopia Management Guidelines',
    'Refractive Error In Children' => 'Refractive Error In Children',
    'Myopia Prevalence' => 'Myopia Prevalence',
    'Signs Of Vision Problem In Kids' => 'Signs Of Vision Problem In Kids',
    'Treatment Of Myopia Control In Children' => 'Treatment Of Myopia Control In Children',
    'Treatment Of Myopia Control In Low Dose Atropine' => 'Treatment Of Myopia Control In Low Dose Atropine',
    'Treatment Of Myopia Control In Spectacle' => 'Treatment Of Myopia Control In Spectacle',
    'Treatment Of Myopia Control In Contact Lenses' => 'Treatment Of Myopia Control In Contact Lenses',
    'Treatment Of Myopia Control In Outdoor Activity' => 'Treatment Of Myopia Control In Outdoor Activity',
    'Covid-19 Lockdown Changed Children’S Eyes' => 'Covid-19 Lockdown Changed Children’S Eyes'
];
?>
<style>
    .help-block {color: red}
    .redactor-editor {min-height: 250px !important;}
</style>

<div class="healthcare-form">

    <?php $form = ActiveForm::begin([
        'options' => [
            'enctype'=>'multipart/form-data',
            'class' => ''
        ]]); ?>

    <?= $form->field($model, 'category')->dropDownList($listData, ['prompt'=>'Choose Category...']) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['maxlength' => true]) ?>

    <div class="row" id="img_row">
        <div class="col-md-6">
            <div class="form-group field-healthcare-img">
                <label class="control-label" for="healthcare-img">Image</label>
                <div class="custom-file">
                    <input type="hidden" name="Healthcare[img]" value="">
                    <input type="file" class="form-control custom-file-input" id="healthcare-img" onchange="putImage()" accept=".png,.jpg,.jpeg" name="Healthcare[img]" aria-describedby="healthcare-img">
                    <label class="custom-file-label" for="healthcare-img">Select file</label>
                </div>
                <div class="help-block"></div>
            </div>

        </div>
        <div class="col-md-6">
            <img id="selected_img" src="<?=Yii::$app->params['custom_url'] . ltrim($model->image,'/') ?>" width="50%" alt="">
        </div>
    </div>

    <?php //= $form->field($model, 'image')->textInput(['maxlength' => true]) ?>

    <h6 class="mt-5">For Detail Page</h6>
    <hr class="footer-btm py-2">

    <?= $form->field($model, 'details')->widget(\yii\redactor\widgets\Redactor::className(),
        [
            'value'=>0,
            'clientOptions'=>
                [
                    'buttons'=>
                        [
                            'formatting', 'html', 'bold', 'italic', 'underline','font', 'lists', 'image', 'horizontalrule','indent','outdent','alignment','orderedlist','unorderedlist'
                        ],
                    'plugins' => ['imagemanager','fontsize'],
//                    'buttons' => [
//                        'formatting', '|', 'bold', 'italic', 'deleted', '|',
//                        'unorderedlist', 'orderedlist', 'outdent', 'indent', '|',
//                        'image', 'table', 'link', '|',
//                        'alignment', '|', 'horizontalrule',
//                        '|', 'underline', '|', 'alignleft', 'aligncenter', 'alignright', 'justify'
//                    ],
                ]
        ]) ?>


    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
