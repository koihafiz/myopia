<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Healthcare */

$this->title = Yii::t('app', 'Update : {name}', [
    'name' => $model->title,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Healthcares'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="healthcare-update container">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
<?php $this->beginBlock('JsBlock') ?>
    <script type="text/javascript">
        $(document).ready(function(){

            document.querySelector('.custom-file-input').addEventListener('change', function (e) {
                var name = document.getElementById("healthcare-img").files[0].name;
                var nextSibling = e.target.nextElementSibling
                nextSibling.innerText = name
            });



        });


        function showImage(src, target) {
            var fr = new FileReader();
            fr.onload = function(){
                target.src = fr.result;
            }
            fr.readAsDataURL(src.files[0]);
        }

        function putImage() {
            var src = document.getElementById("healthcare-img");
            var target = document.getElementById("selected_img");
            showImage(src, target);
        }
    </script>
<?php $this->endBlock()?>