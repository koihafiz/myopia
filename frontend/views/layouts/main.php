<?php

/** @var \yii\web\View $this */
/** @var string $content */

use common\widgets\Alert;
use frontend\assets\AppAsset;
use yii\bootstrap4\Breadcrumbs;
//use yii\bootstrap4\Html;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;

AppAsset::register($this);

// $GLOBALS['Yii::$app->params['custom_url']'] = '/dev/myopia/';

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class=" js flexbox flexboxlegacy canvas canvastext webgl no-touch geolocation postmessage websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers no-applicationcache svg inlinesvg smil svgclippaths">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php $this->registerCsrfMetaTags() ?>

    <meta name="description" content="MAMP is an advocacy group consisting of Ophthalmologists and Optometrists with special interest in creating awareness and support for children with myopia.">
    <meta name="author" content="koihafiz@gmail.com">

    <title><?= Html::encode($this->title) ?></title>
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="<?=Yii::$app->params['custom_url']?>images/favicon.ico" />

    <meta property="og:locale" content="en_US">
    <meta property="og:type" content="website">
    <meta property="og:title" content="Malaysia Advocacy for Myopia Prevention (MAMP) - Malaysia Advocacy for Myopia Prevention (MAMP)">
    <meta property="og:description" content="MAMP is an advocacy group consisting of Ophthalmologists and Optometrists with special interest in creating awareness and support for children with myopia.">
    <meta property="og:url" content="https://www.myopiamalaysia.com/">
    <meta property="og:site_name" content="Malaysia Advocacy for Myopia Prevention (MAMP)">
    <meta property="article:publisher" content="https://www.facebook.com/myopiamalaysia">
    <meta property="article:modified_time" content="2022-05-20T02:39:40+00:00">
    <meta property="og:image" content="https://www.myopiamalaysia.com/wp-content/uploads/2022/05/MAMP.jpg">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:label1" content="Est. reading time">
    <meta name="twitter:data1" content="3 minutes">

    <style>
        .active {
            color: #fff !important;
            background-color: #4799ef;
            border-radius: 5px;
        }
        .footer-socials li a {
            width: 45px;
            height: 45px;
            background: transparent !important;
        }
        .btn {
            font-size: 0.75rem !important;
            padding: 0.55rem 2rem !important;
        }
    </style>
    <?php $this->head() ?>
</head>
<body class="d-flex flex-column h-100">
<?php $this->beginBody() ?>

<header>
    <div class="header-top-bar d-none d-sm-block">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-12">
                    <ul class="top-bar-info list-inline-item pl-0 mb-0">
                        <li class="list-inline-item">
                            <ul class="list-inline d-inline-block">
                                <li class="list-inline-item"><a class="text-white d-inline-block p-0" target="_blank" href="https://www.facebook.com/myopiamalaysia/">
                                        <img src="<?=Yii::$app->params['custom_url']?>icon/fb.png" alt="" width="21px">
                                    </a>
                                </li>
                                <li class="list-inline-item"><a class="text-white d-inline-block p-0" target="_blank" href="https://www.instagram.com/myopiamalaysia/?utm_medium=copy_link">
                                        <img src="<?=Yii::$app->params['custom_url']?>icon/insta.png" alt="" width="21px">
                                    </a>
                                </li>
                                <li class="list-inline-item"><a class="text-white d-inline-block p-0" target="_blank" href="https://www.tiktok.com/@myopiamalaysia">
                                        <img src="<?=Yii::$app->params['custom_url']?>icon/tiktok.png" alt="" width="21px">
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="list-inline-item"><a href="mailto:myopiamalaysia@gmail.com"><i class="icofont-support-faq mr-2"></i>myopiamalaysia@gmail.com</a></li>
                        <li class="list-inline-item"><i class="icofont-location-pin mr-2"></i>Kuala Lumpur, Malaysia </li>
                    </ul>
                </div>
<!--                <div class="col-lg-6">-->
<!--                    <div class="text-lg-right top-right-bar mt-2 mt-lg-0">-->
<!--                        <a href="tel:+23-345-67890" >-->
<!--                            <span>Call Now : </span>-->
<!--                            <span class="h4">823-4565-13456</span>-->
<!--                        </a>-->
<!--                    </div-->
<!--                </div-->
            </div>
        </div>
    </div>
    </div>
    <nav class="navbar navbar-expand-lg navigation" id="navbar">
        <div class="container">
            <a class="navbar-brandxx" href="<?=Yii::$app->params['custom_url']?>index" style="margin-bottom: 19px">
                <img src="<?=Yii::$app->params['custom_url']?>images/logo.png" alt="" class="img-fluid" style="width: 121px;">
            </a>

            <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarmain" aria-controls="navbarmain" aria-expanded="false" aria-label="Toggle navigation">
                <span class="icofont-navigation-menu"></span>
            </button>


            <div class="collapse navbar-collapse" id="navbarmain">
<!--                <ul class="nav nav-pills nav-justified">-->
<!--                    <li class="nav-item">-->
<!--                        <a class="nav-link active" href="#">Active</a>-->
<!--                    </li>-->
<!--                    <li class="nav-item">-->
<!--                        <a class="nav-link" href="#">Link</a>-->
<!--                    </li>-->
<!--                    <li class="nav-item">-->
<!--                        <a class="nav-link" href="#">Link</a>-->
<!--                    </li>-->
<!--                    <li class="nav-item">-->
<!--                        <a class="nav-link disabled" href="#">Disabled</a>-->
<!--                    </li>-->
<!--                </ul>-->
                <ul class="navbar-nav ml-auto" style="line-height: 1.1 !important;">
                    <?php if(Yii::$app->user->isGuest):?>
                        <li class="nav-item">
                            <a class="nav-link <?=Yii::$app->controller->id == 'site' && Yii::$app->controller->action->id == 'index' ? 'active':''?>" href="<?=Yii::$app->params['custom_url']?>index">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link <?=Yii::$app->controller->id == 'site' && Yii::$app->controller->action->id == 'about' ? 'active':''?>" href="<?=Url::to(Yii::$app->params['custom_url'] . 'about')?>">About</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link <?=Yii::$app->controller->id == 'site' && (Yii::$app->controller->action->id == 'education' || Yii::$app->controller->action->id == 'myopia' || Yii::$app->controller->action->id == 'severe' || Yii::$app->controller->action->id == 'risk' || Yii::$app->controller->action->id == 'symptom' || Yii::$app->controller->action->id == 'prevention' || Yii::$app->controller->action->id == 'disease') ? 'active':''?>" href="<?=Url::to(Yii::$app->params['custom_url'] . 'education')?>">Public Education</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link <?=Yii::$app->controller->id == 'site' && Yii::$app->controller->action->id == 'healthcare' ? 'active':''?>" href="<?=Url::to(Yii::$app->params['custom_url'] . 'healthcare')?>">Healthcare Information</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link <?=Yii::$app->controller->id == 'site' && Yii::$app->controller->action->id == 'treatment' ? 'active':''?>" href="<?=Url::to(Yii::$app->params['custom_url'] . 'treatment')?>">Treatment Information</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link <?=Yii::$app->controller->id == 'site' && Yii::$app->controller->action->id == 'news' ? 'active':''?>" href="<?=Url::to(Yii::$app->params['custom_url'] . 'news')?>">News & Activity</a>
                        </li>
                    <?php else :?>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle <?=Yii::$app->controller->id == 'site' && Yii::$app->controller->action->id == 'index' ? 'active':''?>" id="dropdown02" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="<?=Yii::$app->params['custom_url']?>index">Main Menu <i class="icofont-thin-down"></i></a>
                            <ul class="dropdown-menu" aria-labelledby="dropdown02">
                                <li><a class="dropdown-item" href="<?=Url::to(Yii::$app->params['custom_url'])?>">Home</a></li>
                                <li><a class="dropdown-item" href="<?=Url::to(Yii::$app->params['custom_url'] . 'about')?>">About</a></li>
                                <li><a class="dropdown-item" href="<?=Url::to(Yii::$app->params['custom_url'] . 'education')?>">Public Education</a></li>
                                <li><a class="dropdown-item" href="<?=Url::to(Yii::$app->params['custom_url'] . 'healthcare')?>">Healthcare Information</a></li>
                                <li><a class="dropdown-item" href="<?=Url::to(Yii::$app->params['custom_url'] . 'treatment')?>">Treatment Information</a></li>
                                <li><a class="dropdown-item" href="<?=Url::to(Yii::$app->params['custom_url'] . 'news')?>">News & Activity</a></li>
                            </ul>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link <?=Yii::$app->controller->id == 'committee' ? 'active':''?>" href="<?=Url::to(Yii::$app->params['custom_url'] . 'committee/index')?>">Committee</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link <?=Yii::$app->controller->id == 'video' ? 'active':''?>" href="<?=Url::to(Yii::$app->params['custom_url'] . 'video/index')?>">EduVideo</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link <?=Yii::$app->controller->id == 'education' ? 'active':''?>" href="<?=Url::to(Yii::$app->params['custom_url'] . 'education/index')?>">Public Education</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link <?=Yii::$app->controller->id == 'healthcare' ? 'active':''?>" href="<?=Url::to(Yii::$app->params['custom_url'] . 'healthcare/index')?>">Healthcare</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link <?=Yii::$app->controller->id == 'treatment' ? 'active':''?>" href="<?=Url::to(Yii::$app->params['custom_url'] . 'treatment/index')?>">Treatment</a>
                        </li>
                    <?php endif;?>
                    <li class="nav-item">
                        <?php if(Yii::$app->user->isGuest):?>
                            <a class="nav-link <?=Yii::$app->controller->id == 'site' && Yii::$app->controller->action->id == 'login' ? 'active':''?>" href="<?=Url::to(['/site/login'])?>">Admin</a>
                        <?php else:?>
                            <a class="nav-link" href="<?=Url::to(['/site/logout'])?>" data-method="post">
                                Logout(<?=Yii::$app->user->identity->username?>)
                            </a>
                        <?php endif;?>
                    </li>

                </ul>
            </div>
        </div>
    </nav>
</header>

<main role="main" class="flex-shrink-0">
<!--    <div class="container">-->
        <?php //= Breadcrumbs::widget(['links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
<!--    </div>-->
</main>

<?php if(Yii::$app->controller->action->id !== 'login'):?>
<!-- footer Start -->
<footer class="footer section gray-bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 mr-auto col-sm-6">
                <div class="widget mb-5 mb-lg-0">
                    <div class="logo mb-4">
                        <img src="images/logo.png" alt="" class="img-fluid" style="width: 181px">
                    </div>
                    <p>Myopia Malaysia (MAMP) are a special interest group with a shared interest in children myopia management.</p>

                    <ul class="list-inline footer-socials mt-4">
                        <li class="list-inline-item"><a href="https://www.facebook.com/myopiamalaysia/"><img src="<?=Yii::$app->params['custom_url']?>icon/fb.png" alt="" width="33px"></a></li>
                        <li class="list-inline-item"><a href="https://www.instagram.com/myopiamalaysia/?utm_medium=copy_link"><img src="<?=Yii::$app->params['custom_url']?>icon/insta.png" alt="" width="33px"></a></li>
                        <li class="list-inline-item"><a href="https://www.tiktok.com/@myopiamalaysia"><img src="<?=Yii::$app->params['custom_url']?>icon/tiktok.png" alt="" width="33px"></a></li>
                    </ul>
                </div>
            </div>

            <div class="col-lg-4 col-md-6 col-sm-6">
                <div class="widget mb-5 mb-lg-0">
                    <h4 class="text-capitalize mb-3">What We Do</h4>
                    <div class="divider mb-4"></div>

                    <ul class="list-unstyled footer-menu lh-35">
                        <li><a href="<?=Url::to(Yii::$app->params['custom_url'] . 'prevention')?>"><i class="icofont-light-bulb mr-1"></i> Myopia prevention </a></li>
                        <li><a href="<?=Url::to(Yii::$app->params['custom_url'] . 'symptom')?>"><i class="icofont-light-bulb mr-1"></i> Myopia control </a></li>
                        <li><a href="<?=Url::to(Yii::$app->params['custom_url'] . 'myopia')?>"><i class="icofont-light-bulb mr-1"></i> Myopia awareness </a></li>
                        <li><a href="<?=Url::to(Yii::$app->params['custom_url'] . 'education')?>"><i class="icofont-light-bulb mr-1"></i> Public education </a></li>
                        <li><a href="<?=Url::to(Yii::$app->params['custom_url'] . 'healthcare')?>"><i class="icofont-light-bulb mr-1"></i> Healthcare information</a></li>
                    </ul>
                </div>
            </div>


            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="widget widget-contact mb-5 mb-lg-0">
                    <h4 class="text-capitalize mb-3">Get in Touch</h4>
                    <div class="divider mb-4"></div>

                    <div class="footer-contact-block mb-4">
                        <div class="icon d-flex align-items-center">
                            <i class="icofont-home mr-3"></i>
                            <span class="h6 mb-0">Malaysia</span>
                        </div>
                        <!-- <h4 class="mt-2"><a href="tel:+23-345-67890">Support@email.com</a></h4> -->
                    </div>

                    <div class="footer-contact-block">
                        <div class="icon d-flex align-items-center">
                            <a href="mailto:myopiamalaysia@gmail.com">
                                <i class="icofont-email mr-3"></i>
                                <span class="h6 mb-0">myopiamalaysia@gmail.com</span>
                            </a>

                        </div>
                        <!-- <h4 class="mt-2"><a href="tel:+23-345-67890">+23-456-6588</a></h4> -->
                    </div>
                </div>
            </div>
        </div>

        <div class="footer-btm py-4 mt-5">
            <div class="row align-items-center justify-content-between">
                <div class="col-lg-12">
                    <div class="copyright">
                        <p class="float-left">&copy;<?= date('Y') ?> <?= Html::encode(Yii::$app->name) ?>. All rights reserved</p>
                        <p class="float-right"><?php //= Yii::powered() ?></p>

                    </div>
                </div>

            </div>

            <div class="row">
                <div class="col-lg-4">
                    <a class="backtop js-scroll-trigger" href="#top">
                        <i class="icofont-long-arrow-up"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
</footer>
<?php endif;?>



<?php $this->endBody() ?>
<?php if(isset($this->blocks['JsBlock'])):?>
    <?=$this->blocks['JsBlock']?>
<?php endif;?>
</body>
</html>
<?php $this->endPage();

