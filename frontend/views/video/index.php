<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\VideoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Videos');
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    a[title=View] {color: #007bff;}
    a[title=Update] {color: #28a745;}
    a[title=Delete] {color: #dc3545;}
    .img_style {width: 113px}

</style>
<div class="container">

    <h3><?= Html::encode($this->title) ?></h3>

    <p>
        <?= Html::a(Yii::t('app', 'Create Video'), ['create'], ['class' => 'btn btn-success mb-5']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            'title',
            'iframe',
//            [
//                'attribute' => 'src',
//                'contentOptions' => ['class' => 'text-black-600 align-items-center'],
//                'headerOptions' => ['class' => 'fw-bold text-primary'],
//                'format' =>  ['html'],
//                'value' => function($model) {
//                    return $model->src;
//                },
//                'filter' => false,
//            ],
//            'src',
            'created_at:datetime',
            //'updated_at',
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => '<a href="#">Action</a>',
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
