<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use froala\froalaeditor\FroalaEditorWidget;

/* @var $this yii\web\View */
/* @var $model common\models\Education */
/* @var $form yii\widgets\ActiveForm */
?>
<style>
    .redactor-editor {min-height: 250px !important;}
</style>
<div class="education-form">

    <?php $form = ActiveForm::begin([
        'options' => [
            'enctype'=>'multipart/form-data',
            'class' => ''
        ]]); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'short_description')->textInput(['maxlength' => true]) ?>

    <?php //= $form->field($model, 'image')->textInput(['maxlength' => true]) ?>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group field-education-img">
                <label class="control-label" for="education-img">Image</label>
                <div class="custom-file">
                    <input type="hidden" name="Education[img]" value="">
                    <input type="file" class="form-control custom-file-input" id="education-img" onchange="putImage()" accept=".png,.jpg,.jpeg" name="Education[img]" aria-describedby="education-img">
                    <label class="custom-file-label" for="education-img">Select file</label>
                </div>
                <div class="help-block"></div>
            </div>

        </div>
        <div class="col-md-6">
            <img id="selected_img" src="<?=Yii::$app->params['custom_url'] . ltrim($model->image,'/') ?>" width="50%" alt="">
        </div>
    </div>

    <h6 class="mt-5">For Detail Page</h6>
    <hr class="footer-btm py-2">

    <?= $form->field($model, 'details')->widget(\yii\redactor\widgets\Redactor::className(),
        [
            'value'=>0,
            'clientOptions'=>
                [
                    'buttons'=>
                        [
                            'format', 'bold', 'italic', 'underline','font', 'lists', 'image', 'horizontalrule','indent','outdent','alignment','orderedlist','unorderedlist'
                        ],
                    'plugins' => ['imagemanager'],
//                    'buttons' => [
//                        'formatting', '|', 'bold', 'italic', 'deleted', '|',
//                        'unorderedlist', 'orderedlist', 'outdent', 'indent', '|',
//                        'image', 'table', 'link', '|',
//                        'alignment', '|', 'horizontalrule',
//                        '|', 'underline', '|', 'alignleft', 'aligncenter', 'alignright', 'justify'
//                    ],
                ]
        ]) ?>

    <?= FroalaEditorWidget::widget([
        'model' => $model,
        'attribute' => 'author',
        'options' => [
            // html attributes
            'id'=>'content'
        ],
        'clientOptions' => [
            'toolbarInline' => false,
            'toolbarButtons' => ['fullscreen', 'lineheight','bold', 'italic', 'underline', '|', 'paragraphFormat', 'insertImage'],
            'theme' => 'royal', //optional: dark, red, gray, royal
            'language' => 'en_gb' // optional: ar, bs, cs, da, de, en_ca, en_gb, en_us ...
        ]
    ]); ?>


    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
