<?php

/** @var yii\web\View $this */

use yii\helpers\Url;

$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="page-title bg-1" style="background:url('<?=Yii::$app->params['custom_url']?>images/about/banner_03.jpeg');background-size: cover;">
    <div class="overlayx"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="block text-center">
                    <span class="text-white">Help the children in need</span>
                    <h1 class="text-capitalize mb-5 text-lg">Public Education</h1>

                    <!-- <ul class="list-inline breadcumb-nav">
                      <li class="list-inline-item"><a href="index.html" class="text-white">Home</a></li>
                      <li class="list-inline-item"><span class="text-white">/</span></li>
                      <li class="list-inline-item"><a href="#" class="text-white-50">All Department</a></li>
                    </ul> -->
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section department-single">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-7 text-center">
                <div class="section-title" style="margin-bottom:45px">
                    <h2><?=$model->title?></h2>
                    <div class="divider mx-auto my-4"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8">
                <div class="department-content mt-5">
                    <a href="<?=Yii::$app->params['custom_url'] . Url::to('education')?>" class="read-more"><i class="icofont-simple-left mr-2"></i> Back</a>

                    <?=$model->details?>

                    <!--                    <a href="appoinment.html" class="btn btn-main-2 btn-round-full">Make an Appoinment<i class="icofont-simple-right ml-2  "></i></a>-->
                </div>
            </div>

            <div class="col-lg-4">
                <div class="sidebar-widget schedule-widget mt-5">
                    <h5 class="mb-4">Authors</h5>

                    <?=$model->author?>

                </div>
            </div>
        </div>
    </div>
</section>