<?php

/** @var yii\web\View $this */

use yii\helpers\Url;

//$this->title = 'About';
$this->title = Yii::$app->name;
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="page-title bg-1" style="background:url('<?=Yii::$app->params['custom_url']?>images/about/banner_03.jpeg');background-size: cover;">
    <div class="overlayx"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="block text-center">
                    <span class="text-white">Help the children in need</span>
                    <h1 class="text-capitalize mb-5 text-lg">MAMP Introduction</h1>

                    <!-- <ul class="list-inline breadcumb-nav">
                      <li class="list-inline-item"><a href="index.html" class="text-white">Home</a></li>
                      <li class="list-inline-item"><span class="text-white">/</span></li>
                      <li class="list-inline-item"><a href="#" class="text-white-50">About Us</a></li>
                    </ul> -->
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section about-page">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                <h2 class="title-color" style="font-size: 31px;">Malaysia Advocacy for Myopia Prevention (MAMP)</h2>
            </div>
            <div class="col-lg-8">
                <p>Malaysia Advocacy for Myopia Prevention (MAMP) is an initiative by members of the Malaysian Society of Ophthalmologists (MSO) with the Ministry of Health, Universities and Private Hospitals to educate and bring about a public awareness of Myopia or Short Sightedness among children in Malaysia.</p>
                <p>The incidence and prevalence of Myopia around the world including Malaysia has shown a significant rise in the recent decades. The long term impact of myopia related visual health problems to the effected individuals and the society at large cannot be underscored.</p>
                <!-- <img src="<?=Yii::$app->params['custom_url']?>images/about/sign.png" alt="" class="img-fluid"> -->
            </div>
        </div>


    </div>
</section>

<section class="section service gray-bg" style="padding-top: 80px;">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12 text-center">
                <div class="section-title">
                    <h2 style="font-size:31px">MAMP objectives</h2>
                    <div class="divider mx-auto my-4"></div>
                    <ul class="list-unstyled department-service">
                        <li><i class="icofont-check mr-2"></i>MAMP through its website and other initiatives aims to promote ocular health improvement among children. </li>
                        <li><i class="icofont-check mr-2"></i>This will help with the long term goal of myopia prevention and reduction in significant myopia progression in children who are already myopic.</li>
                        <li><i class="icofont-check mr-2"></i>Indeed a “Short Sighted” problem needs a “Far Sighted” solution.</li>
                        <li><i class="icofont-check mr-2"></i>Our hope is that this website with it’s contents together with initiatives of this working group will go a long way in myopia prevention in Malaysia.</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section service gray-bgx" style="padding-top: 80px;">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-7 text-center">
                <div class="section-title">
                    <h2 style="font-size:31px">Our Vision</h2>
                    <div class="divider mx-auto my-4"></div>
                    <p>To raise awareness of myopia through education and collaborative efforts with all partners including eye care professionals, parents, educators, and policy makers</p>
                </div>
            </div>
        </div>


    </div>
</section>

<section class="section service gray-bg" style="padding-top: 80px;">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-7 text-center">
                <div class="section-title">
                    <h2 style="font-size:31px">Our Mission</h2>
                    <div class="divider mx-auto my-4"></div>
                    <p>A reduction in the prevalence of myopia and high myopia in the Malaysian population</p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section service gray-bg" id="whoarewe" style="padding-top: 80px;background: #FFFFFF;">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12 text-center">
                <div class="section-title" style="margin-bottom: 0;">
                    <h2 style="font-size:31px">Who We Are</h2>
                    <div class="divider mx-auto my-4"></div>
                    <p>We are a special interest group with a shared interest in children myopia management. Our group consists of ophthalmologists and optometrists from different eye hospitals and centres. The team members detail are as below</p>
                </div>

            </div>
        </div>
    </div>

    <style>
        .doc_name {font-size: 15px;}
        .committee_avatar {width: 255px !important;height: 295px !important;}
    </style>
    <section class="section doctors" id="doctors">
        <div class="container">

            <div class="row shuffle-wrapper portfolio-gallery shuffle" style="position: relative; overflow: hidden; height: 447px; transition: height 250ms cubic-bezier(0.4, 0, 0.2, 1) 0s;">

                <?php foreach ($committees as $committee):?>

                    <div class="col-lg-3 col-sm-6 col-md-6 mb-4 shuffle-item shuffle-item--visible" data-groups="[&quot;cat1&quot;,&quot;cat2&quot;]" style="position: absolute; top: 0px; left: 0px; visibility: visible; will-change: transform; opacity: 1; transition-duration: 250ms; transition-timing-function: cubic-bezier(0.4, 0, 0.2, 1); transition-property: transform, opacity;">
                        <div class="position-relative doctor-inner-box">
                            <div class="doctor-profile">
                                <div class="doctor-img">
                                    <img src="<?=Yii::$app->params['custom_url']?><?=ltrim($committee->image,'/')?>" alt="<?=$committee->name?>" class="img-fluid w-100">
                                </div>
                            </div>
                            <div class="content mt-3 text-center">
                                <h4 class="mb-0 doc_name"><a href="<?=Url::to(Yii::$app->params['custom_url'].'committee?id='.$committee->id)?>"><?=strlen($committee->name) > 29 ? substr($committee->name,0,29)."..." : $committee->name?></a></h4>
                                <p><?=strlen($committee->role) > 29 ? substr($committee->role,0,29)."..." : $committee->role?></p>
                                <p><a href="<?=Url::to(Yii::$app->params['custom_url'].'committee?id='.$committee->id)?>" style="color: #1486fc;">Read More</a></p>
                            </div>
                        </div>
                    </div>

                <?php endforeach;?>

            </div>

        </div>
    </section>
</section>
