<?php

/** @var yii\web\View $this */
/** @var yii\bootstrap4\ActiveForm $form */
/** @var \frontend\models\ContactForm $model */

use yii\helpers\Url;


//$this->title = 'Contact';
$this->title = Yii::$app->name;
$this->params['breadcrumbs'][] = $this->title;

$mgms = [];
$err = [];
$prevalences = [];
$kids = [];
$child = [];
$dose = [];
$spec = [];
$lense = [];
$activity = [];
$covids = [];
if($model !== null)
{
    foreach ($model as $item)
    {
        if($item->category == 'Myopia Management Guidelines')
            $mgms[] = $item;
        elseif ($item->category == 'Refractive Error In Children')
            $err[] = $item;
        elseif ($item->category == 'Myopia Prevalence')
            $prevalences[] = $item;
        elseif ($item->category == 'Signs Of Vision Problem In Kids')
            $kids[] = $item;
        elseif ($item->category == 'Treatment Of Myopia Control In Children')
            $child[] = $item;
        elseif ($item->category == 'Treatment Of Myopia Control In Low Dose Atropine')
            $child[] = $item;
        elseif ($item->category == 'Treatment Of Myopia Control In Spectaclex')
            $child[] = $item;
        elseif ($item->category == 'Treatment Of Myopia Control In Contact Lenses')
            $child[] = $item;
        elseif ($item->category == 'Treatment Of Myopia Control In Outdoor Activity')
            $child[] = $item;
        elseif ($item->category == 'Covid-19 Lockdown Changed Children’S Eyes')
            $covids[] = $item;
    }
}
?>

<section class="page-title bg-1" style="background:url('<?=Yii::$app->params['custom_url']?>images/about/banner_03.jpeg');background-size: cover;">
    <div class="overlayx"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="block text-center">
                    <span class="text-white">Help the children in need</span>
                    <h1 class="text-capitalize mb-5 text-lg">Healthcare Information</h1>

                    <!-- <ul class="list-inline breadcumb-nav">
                      <li class="list-inline-item"><a href="index.html" class="text-white">Home</a></li>
                      <li class="list-inline-item"><span class="text-white">/</span></li>
                      <li class="list-inline-item"><a href="#" class="text-white-50">All Department</a></li>
                    </ul> -->
                </div>
            </div>
        </div>
    </div>
</section>

<?php if(!empty($mgms)):?>
    <section class="section about-page mgm">
        <?php foreach ($mgms as $mgm):?>
            <div class="container mb-5">
                <div class="row">
                <div class="col-lg-4">
                    <h2 class="title-color"><?=$mgm->title?></h2>
                </div>
                <div class="col-lg-8">
                    <p><?=$mgm->description?></p>
                    <a href="<?=Url::to('site/healthcare-detail?id=' . $mgm->id)?>" class="btn btn-main-2 btn-round-full btn-icon">Learn more<i class="icofont-simple-right ml-3"></i></a>

                </div>
            </div>
            </div>
        <?php endforeach;?>
    </section>
<?php else :?>
    <section class="section about-page mgm">
        <div class="container">
        <div class="row">
            <div class="col-lg-4">
                <h2 class="title-color">Management Guidelines</h2>
            </div>
            <div class="col-lg-8">
                <p>We developed this world first, clinically focused infographic for the American Academy of Optometry (AAO) meeting in Orlando, Florida in late October 2019. In collaboration with the Centre for Ocular Research and Education (CORE), University of Waterloo, Canada.</p>
                <a href="https://www.myopiaprofile.com/management-in-practice/" target="_blank" class="btn btn-main-2 btn-round-full btn-icon">Learn more<i class="icofont-simple-right ml-3"></i></a>

            </div>
        </div>
    </div>
    </section>
<?php endif;?>

<?php if(!empty($err)):?>
    <section class="section service-2 gray-bg">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-7 text-center">
                <div class="section-title">
                    <h2>Refractive Error In Children</h2>
                    <div class="divider mx-auto my-4"></div>
                </div>
            </div>

            <div class="row">
                <?php foreach ($err as $errs):?>
                    <div class="col-lg-4 col-md-4 ">
                    <div class="department-block mb-5">
                        <img src="<?=Yii::$app->params['custom_url']?><?=ltrim($errs->image,'/')?>" alt="" class="img-fluid w-100">
                        <div class="content">
                            <h4 class="mt-4 mb-2 title-color">c</h4>
                            <p class="mb-4"><?=strlen($errs->description) > 71 ? substr($errs->description,0,71)."..." : $errs->description?></p>
                            <a href="<?=Url::to('site/healthcare-detail?id=' . $errs->id)?>" class="read-more">Learn More  <i class="icofont-simple-right ml-2"></i></a>
                        </div>
                    </div>
                </div>
                <?php endforeach;?>

            </div>
        </div>
</section>
<?php else :?>
    <section class="section service-2 gray-bg">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-7 text-center">
                <div class="section-title">
                    <h2>Refractive Error In Children</h2>
                    <div class="divider mx-auto my-4"></div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-6 col-md-6 ">
                    <div class="department-block mb-5">
                        <img src="<?=Yii::$app->params['custom_url']?>images/service/service-1.jpg" alt="" class="img-fluid w-100">
                        <div class="content">
                            <h4 class="mt-4 mb-2 title-color">WSPOS Myopia consensus statement</h4>
                            <p class="mb-4">Myopia Management Guidelines-Infographic <br><br></p>
                            <a href="https://www.wspos.org/wspos-myopia-consensus-statement/" target="_blank" class="read-more">Learn More  <i class="icofont-simple-right ml-2"></i></a>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6 col-md-6">
                    <div class="department-block mb-5">
                        <img src="<?=Yii::$app->params['custom_url']?>images/service/service-2.jpg" alt="" class="img-fluid w-100">
                        <div class="content">
                            <h4 class="mt-4 mb-2  title-color">Refractive Error In Children</h4>
                            <p class="mb-4">WSPOS Myopia consensus statement - Refractive Errors in Children..</p>
                            <a href="https://aapos.org/glossary/refractive-errors-in-children" target="_blank" class="read-more">Learn More <i class="icofont-simple-right ml-2"></i></a>
                        </div>
                    </div>
                </div>


            </div>
        </div>
</section>
<?php endif;?>

<?php if(!empty($prevalences)):?>
    <section class="section service" style="padding-top: 80px;">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12 text-center">
                <div class="section-title" style="margin-bottom: 0;">
                    <h2 style="font-size:31px">Myopia Prevalence</h2>
                    <div class="divider mx-auto my-4"></div>
                    <!-- <p>We are a special interest group with a shared interest in children myopia management. Our group consists of ophthalmologists and optometrists from different eye hospitals and centres. The team members detail are as below</p> -->
                </div>

            </div>
        </div>
    </div>

    <style>
        .doc_name {font-size: 15px;}
    </style>
    <section class="section doctors" style="padding:45px 0">
        <div class="container">

            <div class="row shuffle-wrapper portfolio-gallery shuffle" style="position: relative; overflow: hidden; height: 447px; transition: height 250ms cubic-bezier(0.4, 0, 0.2, 1) 0s;">
                <?php foreach ($prevalences as $prevalence):?>
                    <div class="col-lg-3 col-sm-6 col-md-6 mb-4 shuffle-item shuffle-item--visible" data-groups="[&quot;cat1&quot;,&quot;cat2&quot;]" style="position: absolute; top: 0px; left: 0px; visibility: visible; will-change: transform; opacity: 1; transition-duration: 250ms; transition-timing-function: cubic-bezier(0.4, 0, 0.2, 1); transition-property: transform, opacity;">
                    <div class="position-relative doctor-inner-box">
                        <div class="doctor-profile">
                            <div class="doctor-img">
                                <img src="<?=Yii::$app->params['custom_url']?><?=ltrim($prevalence->image,'/')?>" alt="doctor-image" class="img-fluid w-100">
                            </div>
                        </div>
                        <div class="content mt-3 text-center">
                            <h4 class="mb-0 doc_name"><a href="doctor-single.html"><?=strlen($prevalence->title) > 29 ? substr($prevalence->title,0,29)."..." : $prevalence->title?></a></h4>
                            <p>Analyzed population-based data to better understand myopia rates among schoolchildren in Taiwan..</p>
                            <p><a href="<?=Url::to('site/healthcare-detail?id=' . $prevalence->id)?>" style="color: #1486fc;">Read More</a></p>
                        </div>
                    </div>
                </div>
                <?php endforeach;?>
            </div>
        </div>
    </section>
</section>
<?php else:?>
    <section class="section service" style="padding-top: 80px;">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12 text-center">
                <div class="section-title" style="margin-bottom: 0;">
                    <h2 style="font-size:31px">Myopia Prevalence</h2>
                    <div class="divider mx-auto my-4"></div>
                    <!-- <p>We are a special interest group with a shared interest in children myopia management. Our group consists of ophthalmologists and optometrists from different eye hospitals and centres. The team members detail are as below</p> -->
                </div>

            </div>
        </div>
    </div>

    <style>
        .doc_name {font-size: 15px;}
    </style>
    <section class="section doctors" style="padding:45px 0">
        <div class="container">

            <div class="row shuffle-wrapper portfolio-gallery shuffle" style="position: relative; overflow: hidden; height: 447px; transition: height 250ms cubic-bezier(0.4, 0, 0.2, 1) 0s;">
                <div class="col-lg-3 col-sm-6 col-md-6 mb-4 shuffle-item shuffle-item--visible" data-groups="[&quot;cat1&quot;,&quot;cat2&quot;]" style="position: absolute; top: 0px; left: 0px; visibility: visible; will-change: transform; opacity: 1; transition-duration: 250ms; transition-timing-function: cubic-bezier(0.4, 0, 0.2, 1); transition-property: transform, opacity;">
                    <div class="position-relative doctor-inner-box">
                        <div class="doctor-profile">
                            <div class="doctor-img">
                                <img src="<?=Yii::$app->params['custom_url']?>images/team/1.jpg" alt="doctor-image" class="img-fluid w-100">
                            </div>
                        </div>
                        <div class="content mt-3 text-center">
                            <h4 class="mb-0 doc_name"><a href="doctor-single.html">Myopia Rate Increasing in Taiwanese Children</a></h4>
                            <p>Analyzed population-based data to better understand myopia rates among schoolchildren in Taiwan..</p>
                            <p><a href="https://www.aao.org/eyenet/article/myopia-rate-increasing-in-taiwanese-children" target="_blank" style="color: #1486fc;">Read More</a></p>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-sm-6 col-md-6 mb-4 shuffle-item shuffle-item--visible" data-groups="[&quot;cat2&quot;]" style="position: absolute; top: 0px; left: 0px; visibility: visible; will-change: transform; opacity: 1; transform: translate(285px, 0px) scale(1); transition-duration: 250ms; transition-timing-function: cubic-bezier(0.4, 0, 0.2, 1); transition-property: transform, opacity;">
                    <div class="position-relative doctor-inner-box">
                        <div class="doctor-profile">
                            <div class="doctor-img">
                                <img src="<?=Yii::$app->params['custom_url']?>images/team/2.jpg" alt="doctor-image" class="img-fluid w-100">
                            </div>
                        </div>
                        <div class="content mt-3 text-center">
                            <h4 class="mb-0 doc_name"><a href="doctor-single.html">Epidemiology of Myopia</a></h4>
                            <p>There is a high prevalence of myopia, 80% to 90%, in young adults in East Asia; myopia has become the leading..</p>
                            <p><a href="https://journals.lww.com/apjoo/fulltext/2016/11000/epidemiology_of_myopia.2.aspx" target="_blank" style="color: #1486fc;">Read More</a></p>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-sm-6 col-md-6 mb-4 shuffle-item shuffle-item--visible" data-groups="[&quot;cat3&quot;]" style="position: absolute; top: 0px; left: 0px; visibility: visible; will-change: transform; opacity: 1; transform: translate(570px, 0px) scale(1); transition-duration: 250ms; transition-timing-function: cubic-bezier(0.4, 0, 0.2, 1); transition-property: transform, opacity;">
                    <div class="position-relative doctor-inner-box">
                        <div class="doctor-profile">
                            <div class="doctor-img">
                                <img src="<?=Yii::$app->params['custom_url']?>images/team/3.jpg" alt="doctor-image" class="img-fluid w-100">
                            </div>
                        </div>
                        <div class="content mt-3 text-center">
                            <h4 class="mb-0 doc_name"><a href="doctor-single.html">Global Prevalence of Myopia and High Myopia and Temporal Trends</a></h4>
                            <p>We performed a systematic review and meta-analysis of..</p>
                            <p><a href="https://www.aaojournal.org/article/S0161-6420(16)00025-7/fulltext" target="_blank" style="color: #1486fc;">Read More</a></p>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-sm-6 col-md-6 mb-4 shuffle-item shuffle-item--visible" data-groups="[&quot;cat3&quot;,&quot;cat4&quot;]" style="position: absolute; top: 0px; left: 0px; visibility: visible; will-change: transform; opacity: 1; transform: translate(855px, 0px) scale(1); transition-duration: 250ms; transition-timing-function: cubic-bezier(0.4, 0, 0.2, 1); transition-property: transform, opacity;">
                    <div class="position-relative doctor-inner-box">
                        <div class="doctor-profile">
                            <div class="doctor-img">
                                <img src="<?=Yii::$app->params['custom_url']?>images/team/4.jpg" alt="doctor-image" class="img-fluid w-100">
                            </div>
                        </div>
                        <div class="content mt-3 text-center">
                            <h4 class="mb-0 doc_name"><a href="doctor-single.html">The Prevalence of Myopia and Factors Associated with It</a></h4>
                            <p>A school-based cross-sectional study of children in grades six to nine was conducted in..</p>
                            <p><a href="https://www.ncbi.nlm.nih.gov/pmc/articles/PMC7183771/" target="_blank" style="color: #1486fc;">Read More</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</section>
<?php endif;?>

<?php if(!empty($kids)):?>
<section class="section testimonial-2 gray-bg">
    <div class="container">
        <?php foreach ($kids as $kid):?>
            <div class="row justify-content-center">
            <div class="col-lg-8">
                <div class="section-title text-center">
                    <h2><?=$kid->title?></h2>
                    <div class="divider mx-auto my-4"></div>
                    <p><?=$kid->description?></p>
                    <a href="<?=Url::to('site/healthcare-detail?id=' . $kid->id)?>" class="btn btn-main-2 btn-round-full btn-icon">Learn more<i class="icofont-simple-right ml-3"></i></a>
                </div>
            </div>
        </div>
        <?php endforeach;?>
    </div>
</section>
<?php else:?>
<section class="section testimonial-2 gray-bg">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-8">
                <div class="section-title text-center">
                    <h2>Signs of Vision Problem in Kids</h2>
                    <div class="divider mx-auto my-4"></div>
                    <p>Sometimes parents can tell if their child has a vision problem. Their child may squint or hold reading material very close to their face. They may also complain about things appearing blurry. There are some less obvious signs of vision problems as well.</p>
                    <a href="https://www.aao.org/eye-health/tips-prevention/four-hidden-signs-of-vision-problems-in-kids" target="_blank" class="btn btn-main-2 btn-round-full btn-icon">Learn more<i class="icofont-simple-right ml-3"></i></a>
                </div>
            </div>
        </div>
    </div>
</section>
<?php endif;?>

<?php if(!empty($child)):?>
    <section class="section service" style="padding-top: 80px;">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-12 text-center">
                    <div class="section-title" style="margin-bottom: 0;">
                        <h2 style="font-size:31px">Treatment Of Myopia Control</h2>
                        <div class="divider mx-auto my-4"></div>
                        <!-- <p>We are a special interest group with a shared interest in children myopia management. Our group consists of ophthalmologists and optometrists from different eye hospitals and centres. The team members detail are as below</p> -->
                    </div>

                </div>
            </div>
        </div>

        <style>
            .doc_name {font-size: 15px;}
        </style>
        <section class="section doctors" style="padding:45px 0">
            <div class="container">

                <div class="row shuffle-wrapper portfolio-gallery shuffle" style="position: relative; overflow: hidden; height: 447px; transition: height 250ms cubic-bezier(0.4, 0, 0.2, 1) 0s;">
                    <?php foreach ($child as $childr):?>
                        <div class="col-lg-3 col-sm-6 col-md-6 mb-4 shuffle-item shuffle-item--visible" data-groups="[&quot;cat1&quot;,&quot;cat2&quot;]" style="position: absolute; top: 0px; left: 0px; visibility: visible; will-change: transform; opacity: 1; transition-duration: 250ms; transition-timing-function: cubic-bezier(0.4, 0, 0.2, 1); transition-property: transform, opacity;">
                            <div class="position-relative doctor-inner-box">
                                <div class="doctor-profile">
                                    <div class="doctor-img">
                                        <img src="<?=Yii::$app->params['custom_url']?><?=ltrim($childr->image,'/')?>" alt="doctor-image" class="img-fluid w-100">
                                    </div>
                                </div>
                                <div class="content mt-3 text-center">
                                    <h4 class="mb-0 doc_name"><a href=""><?=strlen($childr->title) > 29 ? substr($childr->title,0,29)."..." : $childr->title?></a></h4>
                                    <p><?=strlen($childr->description) > 29 ? substr($childr->description,0,29)."..." : $childr->description?></p>
                                    <p><a href="<?=Url::to('site/healthcare-detail?id=' . $childr->id)?>" style="color: #1486fc;">Read More</a></p>
                                </div>
                            </div>
                        </div>
                    <?php endforeach;?>
                </div>
            </div>
        </section>
    </section>
<?php endif;?>

<?php if(!empty($covids)):?>
<section class="section testimonial-2">
    <div class="container">
        <?php foreach ($covids as $covid):?>
            <div class="row justify-content-center">
            <div class="col-lg-8">
                <div class="section-title text-center">
                    <h2><?=$covid->title?></h2>
                    <div class="divider mx-auto my-4"></div>
                    <p><?=$covid->description?></p>
                    <a href="<?=Url::to('site/healthcare-detail?id=' . $covid->id)?>" class="btn btn-main-2 btn-round-full btn-icon">Learn more<i class="icofont-simple-right ml-3"></i></a>
                </div>
            </div>
        </div>
        <?php endforeach;?>
    </div>
</section>
<?php else:?>
<section class="section testimonial-2">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-8">
                <div class="section-title text-center">
                    <h2>COVID-19 Lockdown Changed Children's Eyes</h2>
                    <div class="divider mx-auto my-4"></div>
                    <p>Some ophthalmologists around the country are noticing a spike in children’s vision problems as Americans emerge from the COVID-19 pandemic.</p>
                    <a href="https://www.aao.org/eye-health/tips-prevention/covid19-pandemic-myopia-children-eyes" target="_blank" class="btn btn-main-2 btn-round-full btn-icon">Learn more<i class="icofont-simple-right ml-3"></i></a>
                </div>
            </div>
        </div>
    </div>
</section>
<?php endif;?>

