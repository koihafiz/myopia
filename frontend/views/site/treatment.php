<?php

/** @var yii\web\View $this */

use yii\helpers\Url;

//$this->title = 'About';
$this->title = Yii::$app->name;
$this->params['breadcrumbs'][] = $this->title;


$child = [];
$low = [];
$spec = [];
$lense = [];
$outdoor = [];
if($model !== null)
{
    foreach ($model as $item)
    {
        if($item->category == 'Myopia Control In Children')
            $child[] = $item;
        elseif ($item->category == 'Low Dose Atropine')
            $low[] = $item;
        elseif ($item->category == 'Spectacle')
            $spec[] = $item;
        elseif ($item->category == 'Contact Lenses')
            $lense[] = $item;
        elseif ($item->category == 'Outdoor Activities')
            $outdoor[] = $item;
    }
}
?>
<section class="page-title bg-1" style="background:url('<?=Yii::$app->params['custom_url']?>images/about/banner_03.jpeg');background-size: cover;">
    <div class="overlayx"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="block text-center">
                    <span class="text-white">Help the children in need</span>
                    <h1 class="text-capitalize mb-5 text-lg">Treatment Information</h1>

                    <!-- <ul class="list-inline breadcumb-nav">
                      <li class="list-inline-item"><a href="index.html" class="text-white">Home</a></li>
                      <li class="list-inline-item"><span class="text-white">/</span></li>
                      <li class="list-inline-item"><a href="#" class="text-white-50">All Department</a></li>
                    </ul> -->
                </div>
            </div>
        </div>
    </div>
</section>


<?php if(!empty($child)):?>
    <section class="section service-2">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-7 text-center">
                <div class="section-title">
                    <h2>Myopia Control In Children</h2>
                    <div class="divider mx-auto my-4"></div>
                </div>
            </div>
        </div>
        <div class="row justify-content-md-center">
                <?php foreach ($child as $chil):?>
                    <div class="col-lg-4 col-md-6 ">
                    <div class="department-block mb-5">
                        <img src="<?=Yii::$app->params['custom_url']?><?=ltrim($chil->image,'/')?>" alt="" class="img-fluid w-100" style="">
                        <div class="content">
                            <h4 class="mt-4 mb-2 title-color"><?=strlen($chil->title) > 29 ? substr($chil->title,0,29)."..." : $chil->title?></h4>
                            <p class="mb-4"><?=strlen($chil->description) > 69 ? substr($chil->description,0,69)."..." : $chil->description?></p>
                            <a href="<?=Url::to('site/treatment-detail?id=' . $chil->id)?>" class="read-more">Learn More  <i class="icofont-simple-right ml-2"></i></a>
                        </div>
                    </div>
                </div>
                <?php endforeach;?>

            </div>

</section>
<?php else:?>
    <section class="section service-2">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-7 text-center">
                <div class="section-title">
                    <h2>Myopia Control In Children</h2>
                    <div class="divider mx-auto my-4"></div>
                </div>
            </div>

            <div class="row justify-content-md-center">
                <div class="col-lg-4 col-md-6 ">
                    <div class="department-block mb-5">
                        <img src="<?=Yii::$app->params['custom_url']?>images/service/service-1.jpg" alt="" class="img-fluid w-100">
                        <div class="content">
                            <h4 class="mt-4 mb-2 title-color">Treatment of Increasing Myopia in Children</h4>
                            <p class="mb-4">During childhood, myopia (nearsightedness) is typically treated with glasses or contact lenses..</p>
                            <a href="https://aapos.org/glossary/treatment-for-progressive-myopia" target="_blank" class="read-more">Learn More  <i class="icofont-simple-right ml-2"></i></a>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6">
                    <div class="department-block mb-5">
                        <img src="<?=Yii::$app->params['custom_url']?>images/service/service-2.jpg" alt="" class="img-fluid w-100">
                        <div class="content">
                            <h4 class="mt-4 mb-2  title-color">Myopia Control in Children</h4>
                            <p class="mb-4">Studies show myopia is becoming more common among children. While there is no proven direct link, research..</p>
                            <a href="https://www.aao.org/eye-health/diseases/myopia-control-in-children" target="_blank" class="read-more">Learn More <i class="icofont-simple-right ml-2"></i></a>
                        </div>
                    </div>
                </div>


            </div>
        </div>
</section>
<?php endif;?>

<?php if(!empty($low)):?>
    <section class="section service-2 gray-bg">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-7 text-center">
                <div class="section-title">
                    <h2>Low Dose Atropine</h2>
                    <div class="divider mx-auto my-4"></div>
                </div>
            </div>
        </div>
        <div class="row justify-content-md-center">
            <?php foreach ($low as $lows):?>
                <div class="col-lg-4 col-md-6 ">
                    <div class="department-block mb-5">
                        <img src="<?=Yii::$app->params['custom_url']?><?=ltrim($lows->image,'/')?>" alt="" class="img-fluid w-100" style="">
                        <div class="content">
                            <h4 class="mt-4 mb-2 title-color"><?=strlen($lows->title) > 29 ? substr($lows->title,0,29)."..." : $lows->title?></h4>
                            <p class="mb-4"><?=strlen($lows->description) > 69 ? substr($lows->description,0,69)."..." : $lows->description?></p>
                            <a href="<?=Url::to('site/treatment-detail?id=' . $lows->id)?>" class="read-more">Learn More  <i class="icofont-simple-right ml-2"></i></a>
                        </div>
                    </div>
                </div>
            <?php endforeach;?>

        </div>

</section>
<?php else:?>
    <section class="section service-2 gray-bg">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-7 text-center">
                <div class="section-title">
                    <h2>Low Dose Atropine</h2>
                    <div class="divider mx-auto my-4"></div>
                </div>
            </div>

            <div class="row justify-content-md-center">
                <div class="col-lg-4 col-md-6 ">
                    <div class="department-block mb-5">
                        <img src="<?=Yii::$app->params['custom_url']?>images/service/service-1.jpg" alt="" class="img-fluid w-100">
                        <div class="content">
                            <h4 class="mt-4 mb-2 title-color">Atropine Treatment for Myopia</h4>
                            <p class="mb-4">At the Pediatric Ophthalmology Subspecialty Day, Donald Tan, MD, FRCS, FRCOphth, presented an overview..</p>
                            <a href="https://www.aao.org/eyenet/academy-live/detail/atropine-treatment-myopia" target="_blank" class="read-more">Learn More  <i class="icofont-simple-right ml-2"></i></a>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6">
                    <div class="department-block mb-5">
                        <img src="<?=Yii::$app->params['custom_url']?>images/service/service-2.jpg" alt="" class="img-fluid w-100">
                        <div class="content">
                            <h4 class="mt-4 mb-2  title-color">Low concentration atropine exerts an antimyopic effect</h4>
                            <p class="mb-4">Investigators evaluated changes in ocular biometrics among children receiving..</p>
                            <a href="https://www.aao.org/editors-choice/low-concentration-atropine-exerts-antimyopic-effec" target="_blank" class="read-more">Learn More <i class="icofont-simple-right ml-2"></i></a>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6">
                    <div class="department-block mb-5">
                        <img src="<?=Yii::$app->params['custom_url']?>images/service/service-2.jpg" alt="" class="img-fluid w-100">
                        <div class="content">
                            <h4 class="mt-4 mb-2  title-color">Atropine for the Prevention of Myopia Progression</h4>
                            <p class="mb-4">A Report by the American Academy of Ophthalmology Ophthalmic Technology..</p>
                            <a href="https://www.aao.org/ophthalmic-technology-assessment/atropine-prevention-of-myopia-ota-in-press" target="_blank" class="read-more">Learn More <i class="icofont-simple-right ml-2"></i></a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
</section>
<?php endif;?>

<?php if(!empty($spec)):?>
    <section class="section service-2">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-7 text-center">
                <div class="section-title">
                    <h2>Spectacle</h2>
                    <div class="divider mx-auto my-4"></div>
                </div>
            </div>
        </div>
        <div class="row justify-content-md-center">
            <?php foreach ($spec as $specs):?>
                <div class="col-lg-4 col-md-6 ">
                    <div class="department-block mb-5">
                        <img src="<?=Yii::$app->params['custom_url']?><?=ltrim($specs->image,'/')?>" alt="" class="img-fluid w-100" style="">
                        <div class="content">
                            <h4 class="mt-4 mb-2 title-color"><?=strlen($specs->title) > 29 ? substr($specs->title,0,29)."..." : $specs->title?></h4>
                            <p class="mb-4"><?=strlen($specs->description) > 69 ? substr($specs->description,0,69)."..." : $specs->description?></p>
                            <a href="<?=Url::to('site/treatment-detail?id=' . $specs->id)?>" class="read-more">Learn More  <i class="icofont-simple-right ml-2"></i></a>
                        </div>
                    </div>
                </div>
            <?php endforeach;?>

        </div>
</section>
<?php else:?>
    <section class="section service-2">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-7 text-center">
                <div class="section-title">
                    <h2>Spectacle</h2>
                    <div class="divider mx-auto my-4"></div>
                </div>
            </div>

            <div class="row justify-content-md-center">
                <div class="col-lg-4 col-md-6 ">
                    <div class="department-block mb-5">
                        <img src="<?=Yii::$app->params['custom_url']?>images/service/service-1.jpg" alt="" class="img-fluid w-100">
                        <div class="content">
                            <h4 class="mt-4 mb-2 title-color">One-year myopia control efficacy of spectacle lenses</h4>
                            <p class="mb-4">Spectacle lenses with aspherical lenslets effectively slow myopia progression and..</p>
                            <a href="https://bjo.bmj.com/content/early/2021/04/01/bjophthalmol-2020-318367" target="_blank" class="read-more">Learn More  <i class="icofont-simple-right ml-2"></i></a>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6">
                    <div class="department-block mb-5">
                        <img src="<?=Yii::$app->params['custom_url']?>images/service/service-2.jpg" alt="" class="img-fluid w-100">
                        <div class="content">
                            <h4 class="mt-4 mb-2  title-color">Spectacle Lenses Designed to Reduce Progression of Myopia</h4>
                            <p class="mb-4">There were no statistically significant differences in the rate of progression of myopia between..</p>
                            <a href="https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4696394/" target="_blank" class="read-more">Learn More <i class="icofont-simple-right ml-2"></i></a>
                        </div>
                    </div>
                </div>


            </div>
        </div>
</section>
<?php endif;?>

<?php if(!empty($lense)):?>
    <section class="section service-2 gray-bg">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-7 text-center">
                <div class="section-title">
                    <h2>Contact Lenses</h2>
                    <div class="divider mx-auto my-4"></div>
                </div>
            </div>
        </div>
        <div class="row justify-content-md-center">
            <?php foreach ($lense as $lens):?>
                <div class="col-lg-4 col-md-6 ">
                    <div class="department-block mb-5">
                        <img src="<?=Yii::$app->params['custom_url']?><?=ltrim($lens->image,'/')?>" alt="" class="img-fluid w-100" style="">
                        <div class="content">
                            <h4 class="mt-4 mb-2 title-color"><?=strlen($lens->title) > 29 ? substr($lens->title,0,29)."..." : $lens->title?></h4>
                            <p class="mb-4"><?=strlen($lens->description) > 69 ? substr($lens->description,0,69)."..." : $lens->description?></p>
                            <a href="<?=Url::to('site/treatment-detail?id=' . $lens->id)?>" class="read-more">Learn More  <i class="icofont-simple-right ml-2"></i></a>
                        </div>
                    </div>
                </div>
            <?php endforeach;?>

        </div>
    </section>
<?php else:?>
    <section class="section service-2 gray-bg">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-7 text-center">
                <div class="section-title">
                    <h2>Contact Lenses</h2>
                    <div class="divider mx-auto my-4"></div>
                </div>
            </div>

            <div class="row justify-content-md-center">
                <div class="col-lg-4 col-md-6 ">
                    <div class="department-block mb-5">
                        <img src="<?=Yii::$app->params['custom_url']?>images/service/service-1.jpg" alt="" class="img-fluid w-100">
                        <div class="content">
                            <h4 class="mt-4 mb-2 title-color">Beyond Atropine and Ortho-K</h4>
                            <p class="mb-4">Ongoing concern about Ortho-K–related infection has been a major driving force behind the development of a daily-wear soft contact lens for preventing myopia progression..</p>
                            <a href="https://www.aao.org/eyenet/article/beyond-atropine-and-ortho-k" target="_blank" class="read-more">Learn More  <i class="icofont-simple-right ml-2"></i></a>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6">
                    <div class="department-block mb-5">
                        <img src="<?=Yii::$app->params['custom_url']?>images/service/service-2.jpg" alt="" class="img-fluid w-100">
                        <div class="content">
                            <h4 class="mt-4 mb-2  title-color">Orthokeratology for the Prevention of Myopic Progression</h4>
                            <p class="mb-4">This presentation from the Pediatric Ophthalmology Subspecialty Day 2019, Dr. VanderVeen discusses corneal...</p>
                            <a href="https://www.aao.org/annual-meeting-video/orthokeratology-prevention-of-myopic-progression-i" target="_blank" class="read-more">Learn More <i class="icofont-simple-right ml-2"></i></a>
                        </div>
                    </div>
                </div>


            </div>
        </div>
</section>
<?php endif;?>

<?php if(!empty($outdoor)):?>
    <section class="section about-page pb-5">
    <div class="container">
        <?php foreach ($outdoor as $out):?>
            <div class="row mb-5">
            <div class="col-lg-4">
                <h2 class="title-color"><?=$out->title?></h2>
            </div>
            <div class="col-lg-8">
                <p><?=$out->description?></p>
                <a href="<?=Url::to('site/treatment-detail?id=' . $lens->id)?>" class="btn btn-main-2 btn-round-full btn-icon">Learn more<i class="icofont-simple-right ml-3"></i></a>

            </div>
        </div>
        <?php endforeach;?>
    </div>
</section>
<?php else:?>
    <section class="section about-page">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                <h2 class="title-color">Outdoor Activities</h2>
            </div>
            <div class="col-lg-8">
                <p>exposure to strong sunlight may not be required for prevention of myopia. Longer periods of relatively low outdoor light intensity, as in the shade of trees, may be sufficient for the protective effect. Larger studies of longer duration are warranted. </p>
                <a href="https://www.aao.org/eyenet/article/outdoor-activity-and-myopia-in-childrenl" target="_blank" class="btn btn-main-2 btn-round-full btn-icon">Learn more<i class="icofont-simple-right ml-3"></i></a>

            </div>
        </div>
    </div>
</section>
<?php endif;?>

