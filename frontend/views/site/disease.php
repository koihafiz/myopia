<?php

/** @var yii\web\View $this */

use yii\helpers\Url;

$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="page-title bg-1" style="background:url('<?=Yii::$app->params['custom_url']?>images/about/banner_03.jpeg');background-size: cover;">
    <div class="overlayx"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="block text-center">
                    <span class="text-white">Help the children in need</span>
                    <h1 class="text-capitalize mb-5 text-lg">Public Education</h1>

                    <!-- <ul class="list-inline breadcumb-nav">
                      <li class="list-inline-item"><a href="index.html" class="text-white">Home</a></li>
                      <li class="list-inline-item"><span class="text-white">/</span></li>
                      <li class="list-inline-item"><a href="#" class="text-white-50">All Department</a></li>
                    </ul> -->
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section department-single">
    <div class="container">
        <style>
            .sub_title1 {
                font-size: 23px;
                color: #040649;
            }
        </style>
        <div class="row">
            <div class="col-lg-8">
                <div class="department-content mt-5">
                    <a href="<?=Url::to(Yii::$app->params['custom_url'] . 'education')?>" class="read-more"><i class="icofont-simple-left mr-2"></i> Back</a>

                    <h3 class="text-md">1. What Is High Myopia And What Risk Does It Pose To Out Eyesight?</h3>
                    <div class="divider my-4"></div>
                    <p>High myopia is a condition that occurs in eyes that are myopic (shortsighted),  in which the spectacle power is -6.00 D or more. Complications of high myopia are related to structural changes of the eyeball. In patients with high myopia, there is an excessive elongation of the eyeball. Some may progress to outpouching (“staphyloma”) of the eye wall at the back (Figure 1). The risks of sight threatening eye diseases increase exponentially with an increase in spectacle power.</p>

                    <p class="text-center"><img src="<?=Yii::$app->params['custom_url']?>images/about/eyedisis1.png" class="align-items-center" alt=""></p>

                    <p><strong>Figure 1. </strong>Staphyloma is a localized bulge of back wall of the eyeball (blue arrows).</p>

                    <h3 class="text-md">2. What Are The Complications Of High Myopia?</h3>
                    <p class="sub_title1"><strong>a) Retinal Detachment (RD)</strong></p>
                    <p>Structural changes in myopia may lead to thinning of the retina (innermost nerve sensitive tissue of the eye). Due to this change, a hole or break more easily develops in the retina (Figure 2). This allows fluid from the center of the eye called the vitreous, to seep in between the two layers of the retina (the light-sensitive cell layer and the outer pigmented layer). This causes these two layers to separate from each other.</p>
                    <p><strong>The higher the spectacle power, the higher the risk of RD.</strong> Possible symptoms of RD include floaters (black or red moving dots in your vision), flashing lights and loss of vision, usually starting with a “curtain-like” area of blurring in the vision. This condition is considered an emergency, and urgent surgical treatment is needed to prevent irreversible loss of vision.</p>
                    <p class="text-center"><img src="<?=Yii::$app->params['custom_url']?>images/about/eyedisi2.png" alt=""></p>

                    <p><strong>Figure 2. </strong> View of the back of the eye showing detachment of the retina from 8 o’clock to 1 o’clock in the picture in an eye with typical myopic features.</p>

                    <p class="sub_title1"><strong>b) Cataract</strong></p>
                    <p>Cataract is a clouding of the natural lens of the eye that leads to a decrease in vision, usually in a painless and gradual manner (Figure).Cataract is the leading cause of blindness worldwide and it is a condition that occurs naturally  with age. Several studies found that cataract may develop earlier in myopic eyes. Early symptoms of cataract include increasing myopic lens power requirement, blurring of vision despite spectacles, and glare at night as well as decrease in night vision. Cataracts can be treated very effectively with surgery that usually restores vision.</p>
                    <p class="text-center"><img src="<?=Yii::$app->params['custom_url']?>images/about/eyedisi3.png" alt=""></p>
                    <p><strong>Figure 3:</strong> Dense white cataract</p>

                    <p class="sub_title1"><strong>c) Myopic Macular Degeneration (MMD) </strong></p>
                    <p>Patients with pathological myopia may have thinning of the retinal layers in the central part of the retina. This central area of the retina is called the macula, which is responsible for fine, central vision. The long eyeball in highly myopic eyes can develop cracks in its layers causing retinal degeneration. Damage to the macula affects the central vision, which is important for tasks such as reading, writing, driving and recognizing people’s faces and etc. Treatment may be possible when new blood vessels occur in the form of eye injections.</p>

                    <p class="text-center"><img src="<?=Yii::$app->params['custom_url']?>images/about/eysdidi4-1.png" alt=""></p>
                    <p><strong>Figure 5A:</strong> Retina in a eye with myopic macular degeneration and Figure 5B: Normal looking retina. The pictures  highlight the different appearance of MMD eyes compared to normal. This is due to marked degenerative changes that occur in the retina of highly myopic eyes.</p>

                    <p class="sub_title1"><strong>d) Myopic Tractional Maculopathy (MTM)</strong></p>
                    <p>Myopic traction maculopathy (MTM), also known as “myopic foveoschisis” occurs due to stretching and splitting of the retinal layers at the macula. As the eyeball elongates in myopia, the layers of the retina are unable to follow the elasticity of the eye wall’s contour. The central macula may also detach (“foveal detachment”) and can develop a hole at its centre (“myopic macula hole”). MTM requires regular monitoring as it may worsen even when vision is still good.</p>
                    <p>Optical coherence tomography (OCT) is a non-invasive investigation available in most eye specialist clinics, that uses light energy to generate a 2 and 3D image of the macula and optic nerve. OCT of the macula allows MTM in its various stages to be diagnosed by the eye specialist. Treatment should be given before worsening of vision and increased distortion (seeing straight objects as wavy or crooked). MTM may be treated with surgery with variable outcome.</p>

                    <p class="text-center"><img src="<?=Yii::$app->params['custom_url']?>images/about/disis6.png" alt=""></p>

                    <p><strong>Figure 6:</strong> OCT in a patient without myopia showing normal appearance of the macula centre, an area called the fovea.</p>

                    <p class="text-center"><img src="<?=Yii::$app->params['custom_url']?>images/about/disis7.png" alt=""></p>
                    <p><strong>Figure 7:</strong> OCT in patient with myopic foveoschisis showing stretching of the retina layers at the macula due to elongation of the eyeball</p>

                    <p class="sub_title1"><strong>e) Glaucoma</strong></p>
                    <p>Glaucoma is a group of eye conditions that damage the optic nerve. It is one of the leading causes of irreversible blindness for people over the age of 60.  It is also known as the ‘silent thief of sight’ as it has no or mild symptoms until later stages of the disease in the “open-angle” type. Mild symptoms are usually not specific, such as headache or blurred vision in one eye. The risk of “open-angle” glaucoma is higher in persons with myopia. Furthermore, the detection of glaucoma is more difficult due to structural changes of myopia itself. Nevertheless, early detection is crucial because damage to the optic nerve is often permanent and irreversible. Hence, the aim of glaucoma treatment is to preserve remaining eyesight and prevent further loss. Treatment includes life-long eyedrop therapy, laser and when needed, surgery.</p>
                    <p class="text-center"><img src="<?=Yii::$app->params['custom_url']?>images/about/dd10.png" alt=""></p>
                    <p><strong>Figure 9:</strong> This picture shows the result of a test called a visual field test done on one eye of a patient with glaucoma of moderate severity. The visual field is the area you can see with your eye. The dark areas in this field are areas which the patient is unable to see. The light areas are where vision is still intact.</p>

                    <p class="text-center"><img src="<?=Yii::$app->params['custom_url']?>images/about/d10.png" alt=""></p>
                    <p><strong>Figure 10:</strong> Visual field picture showing severe glaucoma which gives the patient a “tunnel” kind of vision; namely only in the centre of the field. Treatment should be given before vision is lost to this level.</p>

                    <p class="sub_title1"><strong>f) Cornea ulcer caused by contact lens wear</strong></p>
                    <p>Myopia is one of the commonest reasons for persons to wear contact lenses as it gives them freedom from their glasses and what some individuals perceive as better appearance. However, contact lens wear comes with responsibility. One of the indirect consequences of myopia include the development of ulceration of the cornea in persons with poor contact lens hygiene (Figure 11). The cornea is the clear, transparent tissue located at the front of the eye, in front of the iris which gives our eyes their colour. Cornea ulceration is a serious and potentially blinding condition that requires urgent treatment to save the eyesight. </p>
                    <p>arly symptoms include eye discomfort, a sensation of dust in the eye and tearing. Mild infection can be treated with antibiotics. Later symptoms include pain, red eye, poor vision and yellowish discharge from the eye. Severe cornea infection can lead to blindness due to a scar developing in the cornea. If the scar is in the centre of the cornea, transplantation of a donor cornea to restore vision may be needed (Figure 12). Good hygiene and avoidance of overwear while using contact lenses is one of the most important factors in preventing infection. </p>
                    <p class="text-center"><img src="<?=Yii::$app->params['custom_url']?>images/about/e1.png" alt=""></p>
                    <p><strong>Figure 11:</strong> Ulceration of the cornea in a contact lens user can be seen as a whitish discolouration of the central part of the eye, in an eye that also looks red.</p>
                    <p class="text-center"><img src="<?=Yii::$app->params['custom_url']?>images/about/e2.png" alt=""></p>
                    <p><strong>Figure 12:</strong> Picture showing the appearance of an eye that has undergone a transplant of new cornea from a donor to restore clear vision. The black lines radiating outwards represent fine stitches to keep the new cornea in place.</p>

                    <h3 class="mt-5 mb-4">3) Worldwide Scale Of The Problem</h3>
                    <p>Worldwide there are almost 1.5 billion people living with myopia and 163 million with high myopia. Even though myopia in most cases can be easily corrected with glasses, contact lenses or surgery, the large and increasing numbers of persons with myopia also mean eye specialists (ophthalmologists) are encountering more complications from myopia. It has been estimated that as many as 10 million people worldwide have vision loss from myopic macular degeneration (MMD) and 3.3 million are  legally blind. This number could rise to 55 million people with impaired vision and 18.5 million blind by 2050, or 0.57% and 0.19% of the world’s population respectively<sup>2</sup></p>

                    <h3 class="mt-5 mb-4">4) Recommendations And Conclusion</h3>
                    <p>Persons with high myopia (power of -6.00D or more) should consider having their eyes screened by an ophthalmologist for the complications mentioned above, even if they experience no symptoms. It is especially important to be aware of the complications of myopia and seek treatment as soon as symptoms occur.
                        <br><strong>Acknowledgments: Eye images from the Imagenet, Department of Ophthalmology, Faculty of Medicine, Universiti Kebangsaan Malaysia</strong></p>

                    <p><strong>References:</strong></p>
                    <ol class="list-unstyledz department-service">
                        <li>Holden BA, Fricke TR, Wilson DA, Jong M, Naidoo KS, Sankaridurg P, Wong TY, Naduvilath TJ, Resnikoff S. Global Prevalence of Myopia and High Myopia and Temporal Trends from 2000 through 2050. Ophthalmology. 2016 May;123(5):1036-42</li>
                        <li>Fricke TR, Jong M, Naidoo KS, Sankaridurg P, Naduvilath TJ, Ho SM, Wong TY, Resnikoff S. Global prevalence of visual impairment associated with myopic macular degeneration and temporal trends from 2000 through 2050: systematic review, meta-analysis and modelling. Br J Ophthalmol. 2018 Jul;102(7):855-862</li>
                    </ol>

<!--                    <a href="appoinment.html" class="btn btn-main-2 btn-round-full">Make an Appoinment<i class="icofont-simple-right ml-2  "></i></a>-->
                </div>
            </div>

            <div class="col-lg-4">
                <div class="sidebar-widget schedule-widget mt-5">
                    <h5 class="mb-4">Authors</h5>

                    <strong>1) Lt Kol (Dr) Ahmad Razif bin Omar</strong>
                    <p class="ml-3">Ophthalmologist (Eye Specialist)</p>
                    <p class="ml-3">96 Hospital Angkatan Tentera Lumut, Pangkalan TLDM Lumut Perak</p>
                    <br>
                    <strong>2) Professor Dr Mae-Lynn Catherine Bastion</strong>
                    <p class="ml-3">Consultant Ophthalmologist and Vitreoretinal Specialist</p>
                    <p class="ml-3">Department of Ophthalmology, Faculty of Medicine, Universiti Kebangsaan Malaysia</p>

                </div>
            </div>
        </div>
    </div>
</section>