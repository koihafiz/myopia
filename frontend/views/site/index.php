<?php

use yii\helpers\Url;

/** @var yii\web\View $this */

$this->title = Yii::$app->name;
?>
<!-- Slider Start -->
<section class="banner" style="background: url('<?=Yii::$app->params['custom_url']?>images/bg/slider-bg-1.jpeg');background-repeat: no-repeat;background-size: cover;">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-12 col-xl-7">
                <div class="block">
                    <div class="divider mb-3"></div>
                    <span class="text-uppercase text-sm letter-spacing " style="">Help the children in need </span>
                    <h2 class="mb-3 mt-3" style="">Toward myopia prevention <br> and ocular health improvement </h2>

                    <!-- <p class="mb-4 pr-5">A repudiandae ipsam labore ipsa voluptatum quidem quae laudantium quisquam aperiam maiores sunt fugit, deserunt rem suscipit placeat.</p> -->
                    <div class="btn-container ">
                        <a href="#" class="btn btn-main-2 btn-icon btn-round-full">join the team <i class="icofont-simple-right ml-2  "></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="features">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="feature-block d-lg-flex">
                    <div class="feature-item mb-5 mb-lg-0">
                        <div class="feature-icon mb-4">
                            <i class="icofont-surgeon-alt"></i>
                        </div>
                        <!-- <span>24 Hours Service</span> -->
                        <h3 class="mb-3">Prevention of vision impairment due to myopia</h3>
                        <!-- <p class="mb-4">Get ALl time support for emergency.We have introduced the principle of family medicine.</p> -->
                        <!-- <a href="appoinment.html" class="btn btn-main btn-round-full">Make a appoinment</a> -->
                    </div>

                    <div class="feature-item mb-5 mb-lg-0">
                        <div class="feature-icon mb-4">
                            <i class="icofont-ui-clock"></i>
                        </div>
                        <!-- <span>Timing schedule</span> -->
                        <h3 class="mb-3">Ocular health improvement through awareness</h3>
                        <!-- <ul class="w-hours list-unstyled">
                            <li class="d-flex justify-content-between">Sun - Wed : <span>8:00 - 17:00</span></li>
                            <li class="d-flex justify-content-between">Thu - Fri : <span>9:00 - 17:00</span></li>
                            <li class="d-flex justify-content-between">Sat - sun : <span>10:00 - 17:00</span></li>
                        </ul> -->
                    </div>

                    <div class="feature-item mb-5 mb-lg-0">
                        <div class="feature-icon mb-4">
                            <i class="icofont-support"></i>
                        </div>
                        <!-- <span>Emegency Cases</span> -->
                        <h3 class="mb-3">Advocate healthy eyes to improve quality of life</h3>
                        <!-- <p>Get ALl time support for emergency.We have introduced the principle of family medicine.Get Conneted with us for any urgency .</p> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="section about">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-4 col-sm-6">
                <div class="about-img">
                    <img src="<?=Yii::$app->params['custom_url']?>images/about/sightday3.png" alt="" class="img-fluid">
                    <img src="<?=Yii::$app->params['custom_url']?>images/about/sightday2.jpeg" alt="" class="img-fluid mt-4">
                </div>
            </div>
            <div class="col-lg-4 col-sm-6">
                <div class="about-img mt-4 mt-lg-0">
                    <img src="<?=Yii::$app->params['custom_url']?>images/about/sightday4.jpg" alt="" class="img-fluid">
                </div>
            </div>
            <div class="col-lg-4">
                <div class="about-content pl-4 mt-4 mt-lg-0">
                    <h2 class="title-color">World Sight Day<br> an annual day of awareness</h2>
                    <p class="mt-4 mb-5">World Sight Day is an annual day of awareness held on the second Thursday of October, to focus global attention on vision impairment.
                    </p>

                    <a href="<?=Url::to(Yii::$app->params['custom_url'] . 'activity')?>" class="btn btn-main-2 btn-round-full btn-icon">More Info<i class="icofont-simple-right ml-3"></i></a>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="cta-section d-none">
    <div class="container">
        <div class="cta position-relative">
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="counter-stat">
                        <i class="icofont-doctor"></i>
                        <span class="h3">58</span>k
                        <p>Happy People</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="counter-stat">
                        <i class="icofont-flag"></i>
                        <span class="h3">700</span>+
                        <p>Surgery Comepleted</p>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="counter-stat">
                        <i class="icofont-badge"></i>
                        <span class="h3">40</span>+
                        <p>Expert Doctors</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="counter-stat">
                        <i class="icofont-globe"></i>
                        <span class="h3">20</span>
                        <p>Worldwide Branch</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section about-page gray-bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                <h2 class="title-color">Who We Are</h2>
            </div>
            <div class="col-lg-8">
                <p>Myopia Malaysia (MAMP) are a special interest group with a shared interest in children myopia management. Our group consists of ophthalmologists and optometrists from different eye hospitals and centers.</p>
                <a href="<?=Url::to(Yii::$app->params['custom_url'] . 'about#whoarewe')?>" class="btn btn-main-2 btn-round-full btn-icon">Our Committee<i class="icofont-simple-right ml-3"></i></a>
            </div>
        </div>
    </div>
</section>
<style>
    .btn {border: 1px solid #e3e3e3;}
</style>
<section class="fetaure-page gray-bg">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-3 col-md-6">
                <div class="about-block-item mb-5 mb-lg-0">
                    <img src="<?=Yii::$app->params['custom_url']?>images/about/about1.jpg" alt="" class="img-fluid w-100" style="">
                    <h4 class="mt-3" style="font-size:1.2rem">What is myopia?</h4>
                    <p><button type="button" class="btn btn-outline-info">Read more</button></p>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="about-block-item mb-5 mb-lg-0">
                    <img src="<?=Yii::$app->params['custom_url']?>images/about/about2.jpg" alt="" class="img-fluid w-100" style="">
                    <h4 class="mt-3" style="font-size:1.2rem">Risk factors for myopia</h4>
                    <p><button type="button" class="btn btn-outline-info">Read more</button></p>
                </div>
            </div>Lets know moreel necessitatibus dolor asperiores illum possimus sint voluptates incidunt molestias nostrum laudantium. Maiores porro cumque quaerat.
            <div class="col-lg-3 col-md-6">
                <div class="about-block-item mb-5 mb-lg-0">
                    <img src="<?=Yii::$app->params['custom_url']?>images/about/about3.jpg" alt="" class="img-fluid w-100" style="">
                    <h4 class="mt-3" style="font-size:1.2rem">Signs & symptoms of myopia</h4>
                    <p><button type="button" class="btn btn-outline-info">Read more</button></p>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="about-block-item">
                    <img src="<?=Yii::$app->params['custom_url']?>images/about/about4.jpg" alt="" class="img-fluid w-100" style="">
                    <h4 class="mt-3" style="font-size:1.2rem">Can we prevent myopia</h4>
                    <p><button type="button" class="btn btn-outline-info">Read more</button></p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section team">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <div class="section-title text-center">
                    <h2 class="mb-4">Educational Video</h2>
                    <div class="divider mx-auto my-4"></div>
                    <!-- <p>Today’s users expect effortless experiences. Don’t let essential people and processes stay stuck in the past. Speed it up, skip the hassles</p> -->
                </div>
            </div>
        </div>
        <style>
            iframe {width: 100% !important;height: 200px !important;}
        </style>

        <div class="row">
            <?php foreach ($videos as $video):?>
                <div class="col-lg-4 col-md-6 col-sm-6">
                <div class="team-block mb-5 mb-lg-0">
                    <div class="elementor-column elementor-col-33 elementor-top-column elementor-element elementor-element-a2fbfe8" data-id="a2fbfe8" data-element_type="column">
                        <div class="elementor-column-wrap elementor-element-populated"><div class="elementor-widget-wrap"><div class="elementor-element elementor-element-24377a2 elementor-aspect-ratio-169 elementor-widget elementor-widget-video" data-id="24377a2" data-element_type="widget" data-settings="{&quot;youtube_url&quot;:&quot;https:\/\/www.youtube.com\/watch?v=6ffjn7VQWzk&quot;,&quot;video_type&quot;:&quot;youtube&quot;,&quot;controls&quot;:&quot;yes&quot;,&quot;aspect_ratio&quot;:&quot;169&quot;}" data-widget_type="video.default"><div class="elementor-widget-container"><div class="elementor-wrapper elementor-fit-aspect-ratio elementor-open-inline">
<!--                                            <iframe class="elementor-video" frameborder="0" allowfullscreen="1" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" title="YouTube video player" width="100%" height="200" src="https://www.youtube.com/embed/6ffjn7VQWzk?controls=1&amp;rel=0&amp;playsinline=0&amp;modestbranding=0&amp;autoplay=0&amp;enablejsapi=1&amp;origin=https%3A%2F%2Fwww.myopiamalaysia.com&amp;widgetid=1" id="widget2" data-gtm-yt-inspected-6="true"></iframe>-->
                                            <?=$video->iframe?>
                                        </div></div></div></div></div></div>

                    <!-- <div class="content">
                        <h4 class="mt-4 mb-0"><a href="doctor-single.html">John Marshal</a></h4>
                        <p>Internist, Emergency Physician</p>
                    </div> -->
                </div>
            </div>
            <?php endforeach;?>

        </div>
    </div>
</section>


<section class="section clients">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-7">
                <div class="section-title text-center">
                    <h2>Partners who support us</h2>
                    <div class="divider mx-auto my-4"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row clients-logo">
            <div class="col-lg-2">
                <div class="client-thumb">
                    <img src="<?=Yii::$app->params['custom_url']?>images/about/partner2.png" alt="" class="img-fluid" width="81px">
                </div>
            </div>
            <div class="col-lg-2">
                <div class="client-thumb">
                    <img src="<?=Yii::$app->params['custom_url']?>images/about/partner1.png" alt="" class="img-fluid">
                </div>
            </div>
            <div class="col-lg-2">
                <div class="client-thumb">
                    <img src="<?=Yii::$app->params['custom_url']?>images/about/partner3.png" alt="" class="img-fluid" width="91px">
                </div>
            </div>
            <div class="col-lg-2">
                <div class="client-thumb">
                    <img src="<?=Yii::$app->params['custom_url']?>images/about/partner2.png" alt="" class="img-fluid" width="81px">
                </div>
            </div>
            <div class="col-lg-2">
                <div class="client-thumb">
                    <img src="<?=Yii::$app->params['custom_url']?>images/about/partner1.png" alt="" class="img-fluid">
                </div>
            </div>
            <div class="col-lg-2">
                <div class="client-thumb">
                    <img src="<?=Yii::$app->params['custom_url']?>images/about/partner3.png" alt="" class="img-fluid" width="91px">
                </div>
            </div>
            <div class="col-lg-2">
                <div class="client-thumb">
                    <img src="<?=Yii::$app->params['custom_url']?>images/about/partner2.png" alt="" class="img-fluid" width="81px">
                </div>
            </div>
            <div class="col-lg-2">
                <div class="client-thumb">
                    <img src="<?=Yii::$app->params['custom_url']?>images/about/partner1.png" alt="" class="img-fluid">
                </div>
            </div>
            <div class="col-lg-2">
                <div class="client-thumb">
                    <img src="<?=Yii::$app->params['custom_url']?>images/about/partner3.png" alt="" class="img-fluid" width="91px">
                </div>
            </div>
            <!-- <div class="col-lg-2">
                <div class="client-thumb">
                    <img src="<?=Yii::$app->params['custom_url']?>images/about/6.png" alt="" class="img-fluid">
                </div>
            </div> -->
        </div>
    </div>
</section>
