<?php

/** @var yii\web\View $this */

use yii\helpers\Url;

//$this->title = 'About';
$this->title = Yii::$app->name;
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="page-title bg-1" style="background:url('<?=Yii::$app->params['custom_url']?>images/about/banner_03.jpeg');background-size: cover;">
    <div class="overlayx"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="block text-center">
                    <span class="text-white">Help the children in need</span>
                    <h1 class="text-capitalize mb-5 text-lg">Committee</h1>

                    <!-- <ul class="list-inline breadcumb-nav">
                      <li class="list-inline-item"><a href="index.html" class="text-white">Home</a></li>
                      <li class="list-inline-item"><span class="text-white">/</span></li>
                      <li class="list-inline-item"><a href="#" class="text-white-50">About Us</a></li>
                    </ul> -->
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section about-page">
    <div class="container">
        <div class="row">
            <a href="<?=Url::to(Yii::$app->params['custom_url'].'about#whoarewe')?>">
                <i class="icofont-simple-left ml-2" style="line-height: 3"></i><button type="button" class="btn btn-link" style="margin-left: -31px !important;">Back</button>
            </a>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-6">
                <div class="doctor-img-block">
                    <img src="<?=Yii::$app->params['custom_url'] . $model->image?>" alt="" class="img-fluid w-100">

                    <div class="info-block mt-4">
                        <h4 class="mb-0"><?=$model->name?></h4>
                        <p><?=$model->role?></p>

<!--                        <ul class="list-inline mt-4 doctor-social-links">-->
<!--                            <li class="list-inline-item"><a href="#"><i class="icofont-facebook"></i></a></li>-->
<!--                            <li class="list-inline-item"><a href="#"><i class="icofont-twitter"></i></a></li>-->
<!--                            <li class="list-inline-item"><a href="#"><i class="icofont-skype"></i></a></li>-->
<!--                            <li class="list-inline-item"><a href="#"><i class="icofont-linkedin"></i></a></li>-->
<!--                            <li class="list-inline-item"><a href="#"><i class="icofont-pinterest"></i></a></li>-->
<!--                        </ul>-->
                    </div>
                </div>
            </div>

            <div class="col-lg-8 col-md-6">
                <div class="doctor-details mt-4 mt-lg-0">
<!--                    <h2 class="text-md">Introducing to myself</h2>-->
                    <div class="divider my-4"></div>
                    <?=$model->description?>
<!--                    <a href="appoinment.html" class="btn btn-main-2 btn-round-full mt-3">Make an Appoinment<i class="icofont-simple-right ml-2  "></i></a>-->
                </div>
            </div>
        </div>
    </div>
</section>
