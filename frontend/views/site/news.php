<?php

/** @var yii\web\View $this */

use yii\helpers\Html;

//$this->title = 'About';
$this->title = Yii::$app->name;
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="page-title bg-1" style="background:url('<?=Yii::$app->params['custom_url']?>images/about/banner_03.jpeg');background-size: cover;">
    <div class="overlayx"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="block text-center">
                    <span class="text-white">Help the children in need</span>
                    <h1 class="text-capitalize mb-5 text-lg">News & Activities</h1>

                    <!-- <ul class="list-inline breadcumb-nav">
                      <li class="list-inline-item"><a href="index.html" class="text-white">Home</a></li>
                      <li class="list-inline-item"><span class="text-white">/</span></li>
                      <li class="list-inline-item"><a href="#" class="text-white-50">About Us</a></li>
                    </ul> -->
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section about-page">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                <h2 class="title-color" style="font-size: 31px;">Outdoor Activity and Myopia Progression in Children</h2>
            </div>
            <div class="col-lg-8">
                <p>Building on evidence of the benefit of outdoor activity for prevention and control of myopia, Wu et al. imple­mented a program to encourage young Taiwanese schoolchildren to spend more time outside. After 1 year, those students who had been encouraged to spend at least 11 hours per week outdoors, with exposure to light intensity of at least 1,000 lux, had signifi­cantly less myopic shift and axial elongation than did those in the control group.</p>
                <!-- <img src="<?=Yii::$app->params['custom_url']?>images/about/sign.png" alt="" class="img-fluid"> -->
                <a href="https://www.aao.org/eyenet/article/outdoor-activity-and-myopia-in-children" target="_blank" class="btn btn-main-2 btn-round-full btn-icon">Learn more<i class="icofont-simple-right ml-3"></i></a>
            </div>
        </div>


    </div>
</section>
