<?php

/** @var yii\web\View $this */

use yii\helpers\Url;

$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="page-title bg-1" style="background:url(<?=Yii::$app->params['custom_url']?>'images/about/banner_03.jpeg');background-size: cover;">
    <div class="overlayx"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="block text-center">
                    <span class="text-white">Help the children in need</span>
                    <h1 class="text-capitalize mb-5 text-lg">Public Education</h1>

                    <!-- <ul class="list-inline breadcumb-nav">
                      <li class="list-inline-item"><a href="index.html" class="text-white">Home</a></li>
                      <li class="list-inline-item"><span class="text-white">/</span></li>
                      <li class="list-inline-item"><a href="#" class="text-white-50">All Department</a></li>
                    </ul> -->
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section department-single">
    <div class="container">

        <div class="row">
            <div class="col-lg-8">
                <div class="department-content mt-5">
                    <a href="<?=Url::to(Yii::$app->params['custom_url'] . 'education')?>" class="read-more"><i class="icofont-simple-left mr-2"></i> Back</a>

                    <h3 class="text-md">The Risk factor For Myopia</h3>
                    <div class="divider my-4"></div>

                    <p>Myopia is one of the most common eye anomalies in the world especially in Asian populations as compared to European and African population1. In Malaysia, the prevalence of myopia in school children was found to be 9.8% for children aged 7 year old and 34.4% for children aged 15 year old based on a WHO sponsored study in Gombak, Malaysia in 2003 2. Today, the prevalence is expected to be higher in keeping with similar trends observed worldwide. Medical studies show that the prevalence of myopia is influenced by <strong>genetic and environmental factors.</strong> </p>

                    <h3 class="mt-5 mb-4">Genetic Factors</h3>
                    <p>Myopia tends to run in families where if one or both parents are myopic, the children are very likely to develop myopia. Research showed that children with both <strong>myopic parents</strong> have a six-fold or higher increase in risk of developing juvenile myopia 3. In addition to myopia onset, research also revealed that the children with at least 1 myopic parent have a higher rate of myopia progression (0.63Diopter/year) compared to children without myopic parents (0.42Diopter/year)<sup>4</sup> . </p>

                    <p><strong>Ethnicity</strong> is another risk factor for progression of myopia. There is higher prevalence and progression of myopia in the Asian population compared to people of European or African descent. In the refractive error study in Gombak, Malaysia, prevalence of myopia is highest among Chinese children (30.9%), followed by Indian children (12.5%) and Malay children (9.2%)<sup>2</sup> . </p>

                    <h3 class="mt-5 mb-4">Environmental Factors </h3>
                    <p>Apart from the genetic factors, <strong>environmental factors</strong> also play an important role in the onset and progression of myopia. One study revealed that the children in urban environments have higher prevalence of myopia compared to those in rural environments despite having similar genetic background. Research in China had found that the myopia prevalence in children aged 5-15 years old in GuangZhou (urban area) was higher at 38.1%<sup>5</sup> compared to 5% in rural areas<sup>6</sup>. The study in Iran showed similar results, with 7.2% in Tehran 7 compared to rural Dezful with 3.4%<sup>8</sup>.
                    </p>
                    <p><strong>Prolonged near work</strong> is another risk factor for myopia onset and progression, where it is believed that the long hours spent on near work such as reading and digital devices will lead to increase in visual stress. The visual stress results in an increase in accommodation, which then contributes to the eyeball elongation and myopia. One research in East Asia had revealed that the onset of myopia increases by 2% for every additional hour spent on near work per week<sup>9</sup>.
                    </p>
                    <p>Besides that, excessive near work also means <strong>reduced time spent outdoors</strong>. The limited time spent outdoors is likely to lead to higher progression of myopia. Study conducted in Taiwan found that school children who spend at least 11 hours for outdoor activities in 1 week have significantly fewer myopia progression<sup>10</sup>. The exposure to sunlight/outdoor light coupled with lesser eye strain from near work help to reduce eyeball axial length elongation, thus retarding myopia progression. </p>

                    <p><strong>References:</strong></p>
                    <ol class="list-unstyledz department-service">
                        <li>Rudnicka AR, Kapetanakis VV, Wathern AK, et al. Global variations and time trend in the prevalence of childhood myopia, a systematic review and quantitative meta-analysis: Implications for aetiology and early prevention. British Journal of Ophthalmology. 2016;100:882-890</li>
                        <li>Goh P.P, Abqariyah Y, Pokharel GP, Ellwein LB. Refractive error and visual impairment in school-age children in Gombak District, Malaysia. Ophthalmology. 2005;112(4):678-685.</li>
                        <li>Recko M, Stahl ED. Childhood myopia: epidemiology, risk factors, and prevention. Mo Med. 2015;112(2):116-121.</li>
                        <li>Saw SM, Nieto FJ, Schein OD, Levy B, Chew SJ. Familial clustering and myopia progression in Singapore school children. Ophthalmic Epidemiol. 2001;8(4):227-36</li>
                        <li>He M, Zeng J, Liu Y, Xu J, Pokharel GP, Ellwein LB. Refractive error and visual impairment in urban children in southern China. Invest Ophthalmol Vis sci 2004;45(3):793-799</li>
                        <li>Morgan A, Young R, Narankhand B, Chen S, Cottriall C, Hosking S. Prevalence rate of myopia in schoolchildren in rural Mongolia. Optom Vis Sci 2006; 83(1): 53-56.</li>
                        <li>Hashemi H, Ftouhi A, Mohammad K. The age and gender specific prevalences of refractive errors in Tehran: The Tehran Eye Study. Ophthalmic Epidemiology.2004;11:213-225.</li>
                        <li>Fatouhi A, Hashemi H, Khabazkhoob M, Mohammad K. The prevalence of refractive errors among schoolchildren in Dezful, Iran. Br J Ophthalmol. 2007;91(3):287-292.</li>
                        <li>Jones-Jordan LA, Sinnott LT, Cotter SA, Kleinstein RN, Manny RE, Mutti DO, Twelker JD, Zadnik K;CLEERE Study Group. Time outdoors, visual activity, and myopia progression in juvenile-onset myopes. Invest Ophthalmol Vis Sci. 2012 Oct1;53(11):7169-75.</li>
                        <li>Wu PC, Chen CT, Lin KK, et al. Myopia prevention and outdoor light intensity in a school-based cluster randomized trial. Ophthalmology. 2018;125(8):1239-1250.</li>
                    </ol>

<!--                    <a href="appoinment.html" class="btn btn-main-2 btn-round-full">Make an Appoinment<i class="icofont-simple-right ml-2  "></i></a>-->
                </div>
            </div>

            <div class="col-lg-4">
                <div class="sidebar-widget schedule-widget mt-5">
                    <h5 class="mb-4">Authors</h5>

                    <strong>1) Ms. Tang Shin Ying</strong>
                    <p class="ml-3">Senior Optometrist</p>
                    <p class="ml-3">International Specialist EYE Centre (ISEC, KL)</p>
                    <br>
                    <strong>2) Dr Choong Yee Fong</strong>
                    <p class="ml-3">Consultant Ophthalmology and Paediatric Specialist</p>
                    <p class="ml-3">Medical Director, International Specialist EYE Centre (ISEC, KL)  </p>

                </div>
            </div>
        </div>
    </div>
</section>