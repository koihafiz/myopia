<?php

/** @var yii\web\View $this */

use yii\helpers\Url;

//$this->title = 'About';
$this->title = Yii::$app->name;
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="page-title bg-1" style="background:url('<?=Yii::$app->params['custom_url']?>images/about/banner_03.jpeg');background-size: cover;">
    <div class="overlayx"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="block text-center">
                    <span class="text-white">Help the children in need</span>
                    <h1 class="text-capitalize mb-5 text-lg">Public Education</h1>

                    <!-- <ul class="list-inline breadcumb-nav">
                      <li class="list-inline-item"><a href="index.html" class="text-white">Home</a></li>
                      <li class="list-inline-item"><span class="text-white">/</span></li>
                      <li class="list-inline-item"><a href="#" class="text-white-50">All Department</a></li>
                    </ul> -->
                </div>
            </div>
        </div>
    </div>
</section>
<style>
    .doc_name {font-size: 15px;}
    /*.committee_avatar {width: 255px !important;height: 295px !important;}*/
</style>
<section class="section service-2">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-7 text-center">
                <div class="section-title">
                    <h2>Myopia</h2>
                    <div class="divider mx-auto my-4"></div>
                </div>
            </div>
        </div>
        <div class="row">
                <?php foreach ($model as $edu):?>
                    <div class="col-md-4">
                    <div class="department-block mb-5">
                        <img src="<?=Yii::$app->params['custom_url']?><?=ltrim($edu->image,'/')?>" alt="" class="img-fluid w-100">
                        <div class="content">
                            <h4 class="mt-4 mb-2 title-color"><?=strlen($edu->title) > 29 ? substr($edu->title,0,29)."..." : $edu->title?></h4>
                            <p><?=strlen($edu->short_description) > 85 ? substr($edu->short_description,0,85)."..." : $edu->short_description?></p>
                            <a href="<?=Url::to(Yii::$app->params['custom_url'].'edetails?id='.$edu->id)?>" class="read-more">Learn More  <i class="icofont-simple-right ml-2"></i></a>
                        </div>
                    </div>
                </div>
                <?php endforeach;?>

            </div>

</section>

<section class="section testimonial-2 gray-bg">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-7">
                <div class="section-title text-center">
                    <h2>When Should Our Children Have an Eye Test?</h2>
                    <div class="divider mx-auto my-4"></div>
                    <p>Any child who begins school years with blurred vision will not be able to cope as well with his peers when they are unable to see what is written on the board clearly. Hence for most children, do let them go for an Eye Test at an optical shop as school begins.  At age 6+ years...</p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section service-2">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-7 text-center">
                <div class="section-title">
                    <h2>Myopia Control & Treatment</h2>
                    <div class="divider mx-auto my-4"></div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-4 col-md-6 ">
                    <div class="department-block mb-5">
                        <img src="<?=Yii::$app->params['custom_url']?>images/service/service-1.jpg" alt="" class="img-fluid w-100">
                        <div class="content">
                            <h4 class="mt-4 mb-2 title-color">Eye Drops</h4>
                            <p class="mb-4">Atropine is a non-selective muscarinic receptor blocker. The exact mechanism of atropine in..</p>
                            <a href="#" class="read-more">Learn More  <i class="icofont-simple-right ml-2"></i></a>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6">
                    <div class="department-block mb-5">
                        <img src="<?=Yii::$app->params['custom_url']?>images/service/service-2.jpg" alt="" class="img-fluid w-100">
                        <div class="content">
                            <h4 class="mt-4 mb-2  title-color">Contact lenses</h4>
                            <p class="mb-4">Contact lenses are widely used as an alternative to glasses to correct the wearers’ refractive error..</p>
                            <a href="#" class="read-more">Learn More <i class="icofont-simple-right ml-2"></i></a>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6">
                    <div class="department-block mb-5">
                        <img src="<?=Yii::$app->params['custom_url']?>images/service/service-3.jpg" alt="" class="img-fluid w-100">
                        <div class="content">
                            <h4 class="mt-4 mb-2 title-color">Spectacles</h4>
                            <p class="mb-4">Spectacles is the main option in treating myopia (shortsightedness) and improve vision..</p>
                            <a href="#" class="read-more">Learn More <i class="icofont-simple-right ml-2"></i></a>
                        </div>
                    </div>
                </div>


                <div class="col-lg-4 col-md-6 ">
                    <div class="department-block  mb-5 mb-lg-0">
                        <img src="<?=Yii::$app->params['custom_url']?>images/service/service-4.jpg" alt="" class="img-fluid w-100">
                        <div class="content">
                            <h4 class="mt-4 mb-2 title-color">Refractive Surgery</h4>
                            <p class="mb-4">Refractive surgery is a procedure where ones refractive error is corrected permanently or..</p>
                            <a href="#" class="read-more">Learn More <i class="icofont-simple-right ml-2"></i></a>
                        </div>
                    </div>
                </div>


            </div>
        </div>
</section>
