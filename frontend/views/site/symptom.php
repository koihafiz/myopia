<?php

/** @var yii\web\View $this */

use yii\helpers\Url;

$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="page-title bg-1" style="background:url('<?=Yii::$app->params['custom_url']?>images/about/banner_03.jpeg');background-size: cover;">
    <div class="overlayx"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="block text-center">
                    <span class="text-white">Help the children in need</span>
                    <h1 class="text-capitalize mb-5 text-lg">Public Education</h1>

                    <!-- <ul class="list-inline breadcumb-nav">
                      <li class="list-inline-item"><a href="index.html" class="text-white">Home</a></li>
                      <li class="list-inline-item"><span class="text-white">/</span></li>
                      <li class="list-inline-item"><a href="#" class="text-white-50">All Department</a></li>
                    </ul> -->
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section department-single">
    <div class="container">

        <div class="row">
            <div class="col-lg-8">
                <div class="department-content mt-5">
                    <a href="<?=Url::to(Yii::$app->params['custom_url'] . 'education')?>" class="read-more"><i class="icofont-simple-left mr-2"></i> Back</a>

                    <h3 class="text-md">Sign & Symptoms of Myopia</h3>
                    <div class="divider my-4"></div>

                    <p><strong>Myopia or nearsightedness</strong> is one of the most common eye problem in children. Things at near are seen clearly, but objects at a distance are blurred in children with myopia. It occurs because the light rays entering the eyes are focused in front of the retina instead of on the retina itself.

                        It is often first detected during childhood and is commonly diagnosed between the early school years through the teens. Myopia tends to run in families.

                        Children with myopia have difficulty seeing things at a distance. However, they may be unaware of their vision problem. So, how do you know if a child has myopia? Signs and symptoms of myopia in children include: </p>

                    <ul>
                        <li>Seem to be unaware of distant objects</li>
                        <li>Squinting or partially closing the eyelids when seeing at a distance</li>
                        <li>Holding books or gadgets too close to the face</li>
                        <li>Sitting too close to the television</li>
                        <li>Frequent headaches </li>
                        <li>Frequent eye rubbing </li>
                        <li>Poor performance in school</li>
                    </ul>

                    <p>If your child is showing any signs and symptoms of myopia, do get their eyes checked by an optometrist or an eye doctor (ophthalmologist). It is important to treat myopia in children early so it would not cause permanent visual impairment.</p>

<!--                    <a href="appoinment.html" class="btn btn-main-2 btn-round-full">Make an Appoinment<i class="icofont-simple-right ml-2  "></i></a>-->
                </div>
            </div>

            <div class="col-lg-4">
                <div class="sidebar-widget schedule-widget mt-5">
                    <h5 class="mb-4">Authors</h5>

                    <strong>1) Dr Safinaz Mohd Khialdin</strong>
                    <p class="ml-3">Consultant Paediatric Ophthalmologist & Medical Lecturer </p>
                    <p class="ml-3">Universiti Kebangsaan Malaysia Medical Centre</p>
                </div>
            </div>
        </div>
    </div>
</section>