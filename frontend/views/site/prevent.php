<?php

/** @var yii\web\View $this */

use yii\helpers\Url;

$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="page-title bg-1" style="background:url('<?=Yii::$app->params['custom_url']?>images/about/banner_03.jpeg');background-size: cover;">
    <div class="overlayx"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="block text-center">
                    <span class="text-white">Help the children in need</span>
                    <h1 class="text-capitalize mb-5 text-lg">Public Education</h1>

                    <!-- <ul class="list-inline breadcumb-nav">
                      <li class="list-inline-item"><a href="index.html" class="text-white">Home</a></li>
                      <li class="list-inline-item"><span class="text-white">/</span></li>
                      <li class="list-inline-item"><a href="#" class="text-white-50">All Department</a></li>
                    </ul> -->
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section department-single">
    <div class="container">

        <div class="row">
            <div class="col-lg-8">
                <div class="department-content mt-5">
                    <a href="<?=Url::to(Yii::$app->params['custom_url'] . 'education')?>" class="read-more"><i class="icofont-simple-left mr-2"></i> Back</a>

                    <h3 class="text-md">Can We Prevent Myopia?</h3>
                    <div class="divider my-4"></div>

                    <h3 class="mt-5 mb-4">Introduction</h3>
                    <p>Myopia is a main public eye health problem, particularly in East Asia with prevalence reported to be increasing every year. It was reported that up to 80% of primary school students have myopia. In this emerging eye problem, Malaysia is no exception. Myopia is not only affecting vision but economically and also our social well-being. Global productivity loss (direct and indirect costs) due to vision impairment from complication of high myopia such as myopic macular degeneration and uncorrected myopia was an estimated US$250<sup>1</sup>. </p>
                    <p>Governments and non-governmental organizations (NGOs) need to work as a team and collaborate, especially the Ministry of Education and Ministry of Health in developing a national myopia prevention programme. In 2019, the WHO Regional Office for the Western Pacific, the International Agency for the Prevention of Blindness (IAPB) and the Brien Holden Vision Institute<sup>2</sup>, recommends policy implementation to prevent and control myopia which among others is to encourage outdoor time of 2–3 hours per day for schoolchildren as a form of environmental interventions.</p>

                    <h3 class="mt-5 mb-4">Environmental Interventions</h3>
                    <p>There are two main environmental interventions:</p>
                    <ol>
                        <li>Outdoor activities </li>
                        <li>Near work activity</li>
                    </ol>
                    <p><strong><u>Outdoor activities</u></strong><br>
                    Many studies have shown the importance of increasing outdoor hours to control and prevent myopia<sup>3-9</sup></p>
                    <ul>
                        <li>Sydney Myopia Study, Orinda study and the Singapore Cohort Study of Risk Factors for Myopia, indicates increased time spent outdoors was associated with preventing the onset of myopia.</li>
                        <li>In Guangzhou, an additional 40 minutes being outdoor, reduced the incidence of myopia by 23% among children. Also, every additional hour of outdoor time weekly appeared to decrease the chanced of getting myopia by 2%. </li>
                        <li>In Taiwan, a school-based programme conducted showed that children who spent at least 11 hours outdoors a week had a 54% lower risk of increasing myopia (spectacle power) than children who did not. </li>
                    </ul>
                    <p><strong><u>Near work activity</u></strong><br>
                    Several studies have shown the role of near work as one of the factor to cause myopia<sup>10-14</sup>. In one study, the risk for myopia is increased by 2% for every one diopter-hour more of near work spent every week. </p>
                    <p>Apart from total duration near work, the type of near work is also important.  For example, continuous reading of more than 30–45 min without a rest is more harmful than the total duration of near work itself.</p>

                    <h3 class="mt-5 mb-4">Challenges</h3>
                    <p>There are challenges in increasing outdoor time in children to prevent and reduce myopia onset. This intervention is difficult to be implemented in countries with educational achievement pressure, including Malaysia. This academic-achiever oriented culture leads to children spending more time on near work and less time outdoors as young as preschool age. </p>
                    <p>For example, parents are worried that increased outdoor time may be affecting their academic learning time. Additionally, teachers may not be prepared in conducting classes activity outdoor and incorporating learning with playing outside class. Therefore, the Education and Health Ministry can plan a multiprong strategy via highlighting other health benefits of outdoor activities such as reducing obesity among children and improved mental health. A good exemplary from our neighbour, the Singapore Health Promotion Board has been encouraging families to spend time together at parks and playgrounds as well as participating in outdoor physical activities that can achieve a number of public health goals at once. Interesting as it sounds, the Early Childhood Development Agency in Singapore requires early childhood schools such as kindergartens to include 30–60 minutes of outdoor learning in order for them to obtain operating license.</p>
                    <h3 class="mt-5 mb-4">Summary</h3>
                    <p>In conclusion, increased outdoor time is a priority to prevent myopia. Two  hours of outdoor activity should be incorporated into children’s life daily life. To achieve this outdoor time recommendation, a holistic approach is needed in our country; via strategic collaboration, cooperation and coordination. This requires community (parents/caregivers), ministry, clinicians, educators working together with mutual aim that is to increase comprehensive child health and well-being from a public health perspective.</p>

                    <p><strong>References:</strong></p>
                    <ol class="list-unstyledz department-service">
                        <li>Naidoo KS, Fricke TR, Frick KD, et al. Potential lost productivity resulting from the global burden of myopia: systematic review, meta-analysis, and modeling. Ophthalmology 2019;126:338–46.</li>
                        <li>Ang M, Flanagan JL, Wong CW, et al. Review: Myopia control strategies recommendations from the 2018 WHO/IAPB/BHVI Meeting on Myopia British Journal of Ophthalmology 2020;104:1482-1487.</li>
                        <li>Lingham G, Mackey DA, Lucas R, et al. How does spending time outdoors protect against myopia? A review. Br J Ophthalmol 2019. doi:10.1136/bjophthalmol-2019- 314675. [Epub ahead of print: 13 Nov 2019].</li>
                        <li>Mutti DO, Mitchell GL, Moeschberger ML, et al. Parental myopia, near work, school achievement, and children’s refractive error. Invest Ophthalmol Vis Sci 2002;43:3633–40. 13 </li>
                        <li>Rose KA, Morgan IG, Ip J, et al. Outdoor activity reduces the prevalence of myopia in children. Ophthalmology 2008;115:1279–85. 14 Dirani M, Tong L, Gazzard G, et al. Outdoor activity and myopia in Singapore teenage children. Br J Ophthalmol 2009;93:997–1000.</li>
                        <li>Pärssinen O, Lyyra AL. Myopia and myopic progression among schoolchildren: a three-year follow-up study. Invest Ophthalmol Vis Sci 1993;34:2794–802. </li>
                        <li>He M, Xiang F, Zeng Y, et al. Effect of time spent outdoors at school on the development of myopia among children in China: a randomized clinical trial. JAMA 2015;314:1142–8.</li>
                        <li>Sherwin JC, Reacher MH, Keogh RH, et al. The association between time spent outdoors and myopia in children and adolescents: a systematic review and meta-analysis. Ophthalmology 2012;119:2141–51.</li>
                        <li>Wu P-C, Chen C-T, Lin K-K, et al. Myopia prevention and outdoor light intensity in a school-based cluster randomized trial. Ophthalmology 2018;125:1239–50.</li>
                        <li>Dirani M, Crowston JG, Wong TY. From reading books to increased smart device screen time. Br J Ophthalmol 2019;103:1–2. 20 </li>
                        <li>Huang H-M, Chang DS-T, Wu P-C. The association between near work activities and myopia in Children-A systematic review and meta-analysis. PLoS One 2015;10: e0140419. 21 </li>
                        <li>Huang P-C, Hsiao Y-C, Tsai C-Y, et al. Protective behaviours of near work and time outdoors in myopia prevalence and progression in myopic children: a 2-year prospective population study. Br J Ophthalmol 2019. doi:10.1136/ bjophthalmol-2019-314101. </li>
                        <li>Li S-M, Li S-Y, Kang M-T, et al. Near work related parameters and myopia in Chinese children: the Anyang childhood eye study. PLoS One 2015;10:e0134514. </li>
                        <li>Ip JM, Saw S-M, Rose KA, et al. Role of near work in myopia: findings in a sample of Australian school children. Invest Ophthalmol Vis Sci 2008;49:2903–10
                        </li>
                    </ol>

<!--                    <a href="appoinment.html" class="btn btn-main-2 btn-round-full">Make an Appoinment<i class="icofont-simple-right ml-2  "></i></a>-->
                </div>
            </div>

            <div class="col-lg-4">
                <div class="sidebar-widget schedule-widget mt-5">
                    <h5 class="mb-4">Authors</h5>

                    <strong>1) Dr Duratul Ain bt Hussin</strong>
                    <p class="ml-3">Senior Optometrist (PhD Public Eye Health)</p>
                    <p class="ml-3">Allied Health Sciences Division, Ministry of Health Malaysia</p>
                </div>
            </div>
        </div>
    </div>
</section>