<?php

/** @var yii\web\View $this */

use yii\helpers\Url;

$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="page-title bg-1" style="background:url('<?=Yii::$app->params['custom_url']?>images/about/banner_03.jpeg');background-size: cover;">
    <div class="overlayx"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="block text-center">
                    <span class="text-white">Help the children in need</span>
                    <h1 class="text-capitalize mb-5 text-lg">Public Education</h1>

                    <!-- <ul class="list-inline breadcumb-nav">
                      <li class="list-inline-item"><a href="index.html" class="text-white">Home</a></li>
                      <li class="list-inline-item"><span class="text-white">/</span></li>
                      <li class="list-inline-item"><a href="#" class="text-white-50">All Department</a></li>
                    </ul> -->
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section department-single">
    <div class="container">

        <div class="row">
            <div class="col-lg-8">
                <div class="department-content mt-5">
                    <a href="<?=Url::to('/education')?>" class="read-more"><i class="icofont-simple-left mr-2"></i> Back</a>

                    <h3 class="text-md">What is Myopia (Short-Sightedness)?</h3>
                    <div class="divider my-4"></div>
                    <p class="lead">Myopia is also known as short-sightedness, or the inability to see objects clearly at a distance. </p>
                    <p>The reason objects are blurry in the distance with short-sightedness is because the rays of light that enter the eye are focused in front of the retina (light sensitive nerve tissue at the back of the eye) because the eyeball is too big or long, or the focusing power is too high. The most common way to correct this is with glasses or contact lenses which refocus the light onto the retina.</p>
                    <p><strong>Figure 1. </strong>Diagram showing the basic structure of our eye. Diagram courtesy of Dr Chow Jun Yong, Registrar in training, Hospital Tengku Ampuan Afzan, Kuantan, Ministry of Health Malaysia).</p>
                    <p class="text-center"><img src="<?=Yii::$app->params['custom_url']?>images/about/myopiaf1.png" class="align-items-center" alt=""></p>

                    <p><strong>Figure 2A. </strong>Diagram illustrating that the light rays entering the eye in a patient with myopia are not focused onto the retina but focused to a point in front of the retina (f). It can be seen that f forms in front of the retina due to the eyeball being too long.</p>
                    <p class="text-center"><img src="<?=Yii::$app->params['custom_url']?>images/about/f2.png" alt=""></p>

                    <p><strong>Figure 2B. </strong>When a corrective lens is placed in front of the myopic eye, f moves to the retina surface allowing a clear image.</p>
                    <p class="text-center"><img src="<?=Yii::$app->params['custom_url']?>images/about/f3.png" alt=""></p>

                    <h3 class="mt-5 mb-4">Population Prevalence</h3>
                    <p>Globally, myopia is estimated to affect 1.5 billion people and the numbers are continuing to increase in epidemic proportions. In Malaysia, myopia is found to be especially common among the Chinese and Malay communities, with a 42 per cent and 15 per cent prevalence rate respectively.1,2,3 ’’ There appears to also be an increase over time in the rate of myopia. For instance, while only one in ten 7-year-old Chinese children had myopia, this rose to one in three by the time they reached their teen years in an area in Selangor, Malaysia in 2005. Urban areas in Malaysia also seem to have higher myopia rates than suburban areas where 15.4% Malay children aged 6 to 12 years living in a suburban area in 2008 had myopia compared to rates 3 times higher in Malay children living in an urban area.</p>
                    <p class="text-center"><img src="<?=Yii::$app->params['custom_url']?>images/about/f4.png" width="331px" alt=""></p>

                    <h3 class="mt-5 mb-4">What Causes Myopia?</h3>
                    <p>Myopia occurs due to an excessive elongation of the eyeball, typically during the growing years of childhood but it can develop at any age. Once myopia begins to develop it usually continues to get worse until young adulthood. We now have more information about the causative factors of myopia than previously. Evidence is strong for multiple factors causing myopia. In particular,   First degree relative (eg father, mother or sibling) with myopia, Asian ethnic group, and environment (particularly living in cities and spending a lot of time indoors), and spending a lot of time carrying out close-up tasks (including higher education background).</p>
<!--                    <div class="divider my-4"></div>-->
                    <p><strong>References:</strong></p>
                    <ol class="list-unstyledz department-service">
                        <li>Chung KM, Mohidin N, Yeow PT, et al. Prevalence of visual disorder in Chinese schoolchildren. Optom Vis Sci 1996; 73:695-700.</li>
                        <li>Garner LF, Mohidin N, Chung KM, et al. Sains Malaysiana 1987; 16:339-346 (in Malay).</li>
                        <li>Holden BA, Wilson DA, Jong M, et al. Myopia: a growing global problem with sight-threatening complications. Community Eye Health. 2015;28(90):35.</li>
                        <li>Goh P-P, Abqariyah Y, Pokharel GP, Ellwein LB. Refractive error and visual impairment in school-age children in Gombak District, Malaysia. Ophthalmology. 2005;112(4):678–685.</li>
                        <li>Hashim S-E, Tan H-K, Wan-Hazabbah W, Ibrahim M. Prevalence of refractive error in malay primary school children in suburban area of Kota Bharu, Kelantan, Malaysia. Ann Acad Med Singap. 2008;37(11):940.</li>
                        <li>Flitcroft DI. The complex interactions of retinal, optical and environmental factors in myopia aetiology. Prog Retin Eye Res. 2012 Nov;31(6):622-60. doi: 10.1016/j.preteyeres.2012.06.004. Epub 2012 Jul 4. PMID: 22772022.</li>
                    </ol>

<!--                    <a href="appoinment.html" class="btn btn-main-2 btn-round-full">Make an Appoinment<i class="icofont-simple-right ml-2  "></i></a>-->
                </div>
            </div>

            <div class="col-lg-4">
                <div class="sidebar-widget schedule-widget mt-5">
                    <h5 class="mb-4">Authors</h5>

                    <strong>1) Lt Kol (Dr) Ahmad Razif bin Omar</strong>
                    <p class="ml-3">Ophthalmologist (Eye Specialist)</p>
                    <p class="ml-3">96 Hospital Angkatan Tentera Lumut, Pangkalan TLDM Lumut Perak</p>
                    <br>
                    <strong>2) Professor Dr Mae-Lynn Catherine Bastion</strong>
                    <p class="ml-3">Consultant Ophthalmologist and Vitreoretinal Specialist</p>
                    <p class="ml-3">Department of Ophthalmology, Faculty of Medicine, Universiti Kebangsaan Malaysia</p>

                </div>
            </div>
        </div>
    </div>
</section>