<?php

/** @var yii\web\View $this */

use yii\helpers\Html;

$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="page-title bg-1" style="background:url('<?=Yii::$app->params['custom_url']?>images/about/banner_03.jpeg');background-size: cover;">
    <div class="overlayx"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="block text-center">
                    <span class="text-white">Help the children in need</span>
                    <h1 class="text-capitalize mb-5 text-lg">MAMP Introduction</h1>

                    <!-- <ul class="list-inline breadcumb-nav">
                      <li class="list-inline-item"><a href="index.html" class="text-white">Home</a></li>
                      <li class="list-inline-item"><span class="text-white">/</span></li>
                      <li class="list-inline-item"><a href="#" class="text-white-50">About Us</a></li>
                    </ul> -->
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section about-page">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <h2 class="title-color" style="font-size: 31px;">WORLD SIGHT DAY 2022</h2>
                <p style="margin-bottom: .3rem">World Sight Day is an annual day of awareness held on the second Thursday of October, to focus global attention on vision impairment.</p>
                <p style="margin-bottom: .3rem">This year, World Sight Day will take place on October 14, 2021 with the theme: Love Your Eyes.</p>
                <p style="margin-bottom: .3rem">In view of this, MAMP initiatives are to share a series of educational talk, educational video animation and educational posts through our social media platforms and media channels.</p>
                <p style="margin-bottom: .3rem">oin us throughout the week and see things differently!</p>
            </div>
            <div class="col-lg-6">
                <img src="<?=Yii::$app->params['custom_url']?>images/about/wsd2022.jpg" class="" width="100%">
                <!-- <img src="<?=Yii::$app->params['custom_url']?>images/about/sign.png" alt="" class="img-fluid"> -->
            </div>
        </div>


    </div>
</section>
<section class="section service gray-bg" id="whoarewe" style="padding-top: 80px;background: #FFFFFF;">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12 text-center">
                <div class="section-title" style="margin-bottom: 0;">
                    <h2 style="font-size:31px">WORLD SIGHT DAY PROGRAMME</h2>
                    <div class="divider mx-auto my-4"></div>
                </div>

            </div>
        </div>
    </div>

    <style>
        .doc_name {font-size: 15px;}
    </style>
    <section class="section doctors">
        <div class="container">

            <div class="row">
                <div class="col-lg-4 col-md-6 ">
                    <div class="department-block mb-5">
                        <img src="<?=Yii::$app->params['custom_url']?>images/activity/1.png" alt="" class="img-fluid w-100">
                        <div class="content">
                            <h4 class="mt-4 mb-2 title-color pl-0">Myopia (Short-Sighted) : What About It</h4>
<!--                            <p class="mb-4">Saepe nulla praesentium eaque omnis perferendis a doloremque.</p>-->
                            <a href="https://www.youtube.com/watch?v=a62OpoY5cng" target="_blank" class="read-more">Learn More  <i class="icofont-simple-right ml-2"></i></a>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6">
                    <div class="department-block mb-5">
                        <img src="<?=Yii::$app->params['custom_url']?>images/activity/2.png" alt="" class="img-fluid w-100">
                        <div class="content">
                            <h4 class="mt-4 mb-2  title-color pl-0">Myopia (Rabun Jauh) : Apa Yang Anda Perlu Tahu?</h4>
<!--                            <p class="mb-4">Saepe nulla praesentium eaque omnis perferendis a doloremque.</p>-->
                            <a href="https://www.youtube.com/watch?v=7ZclHMDAv6I" target="_blank" class="read-more">Learn More <i class="icofont-simple-right ml-2"></i></a>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6">
                    <div class="department-block mb-5">
                        <img src="<?=Yii::$app->params['custom_url']?>images/activity/3.png" alt="" class="img-fluid w-100">
                        <div class="content">
                            <h4 class="mt-4 mb-2 title-color pl-0">Myopia (Rabun Jauh) : Ada Penawar?</h4>
<!--                            <p class="mb-4">Saepe nulla praesentium eaque omnis perferendis a doloremque.</p>-->
                            <a href="https://www.facebook.com/myopiamalaysia/videos/4037291829709693" target="_blank" class="read-more">Learn More <i class="icofont-simple-right ml-2"></i></a>
                        </div>
                    </div>
                </div>


                <div class="col-lg-4 col-md-6 ">
                    <div class="department-block  mb-5 mb-lg-0">
                        <img src="<?=Yii::$app->params['custom_url']?>images/activity/4.png" alt="" class="img-fluid w-100">
                        <div class="content">
                            <h4 class="mt-4 mb-2 title-color pl-0">Myopia (Short-Sighted) : Is There A Cure</h4>
<!--                            <p class="mb-4">Saepe nulla praesentium eaque omnis perferendis a doloremque.</p>-->
                            <a href="https://www.facebook.com/myopiamalaysia/videos/266539572048913" target="_blank" class="read-more">Learn More <i class="icofont-simple-right ml-2"></i></a>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6">
                    <div class="department-block mb-5 mb-lg-0">
                        <img src="<?=Yii::$app->params['custom_url']?>images/activity/5.png" alt="" class="img-fluid w-100">
                        <div class="content">
                            <h4 class="mt-4 mb-2 title-color pl-0">A Short-Sighted Problem Needs Far Sighted Solutions</h4>
<!--                            <p class="mb-4">Saepe nulla praesentium eaque omnis perferendis a doloremque.</p>-->
                            <a href="https://www.facebook.com/myopiamalaysia/videos/1478301079207030/" target="_blank" class="read-more">Learn More <i class="icofont-simple-right ml-2"></i></a>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6">
                    <div class="department-block mb-5 mb-lg-0">
                        <img src="<?=Yii::$app->params['custom_url']?>images/activity/6.png" alt="" class="img-fluid w-100">
                        <div class="content">
                            <h4 class="mt-4 mb-2 title-color pl-0">The Effect Of Lockdown On Children's Eye With DrNorazah</h4>
<!--                            <p class="mb-4">Saepe nulla praesentium eaque omnis perferendis a doloremque.</p>-->
                            <a href="https://www.youtube.com/watch?v=h9I6tVbhwz8" target="_blank" class="read-more">Learn More <i class="icofont-simple-right ml-2"></i></a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
</section>
