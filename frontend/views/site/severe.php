<?php

/** @var yii\web\View $this */

use yii\helpers\Url;

$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="page-title bg-1" style="background:url(<?=Yii::$app->params['custom_url']?>'images/about/banner_03.jpeg');background-size: cover;">
    <div class="overlayx"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="block text-center">
                    <span class="text-white">Help the children in need</span>
                    <h1 class="text-capitalize mb-5 text-lg">Public Education</h1>

                    <!-- <ul class="list-inline breadcumb-nav">
                      <li class="list-inline-item"><a href="index.html" class="text-white">Home</a></li>
                      <li class="list-inline-item"><span class="text-white">/</span></li>
                      <li class="list-inline-item"><a href="#" class="text-white-50">All Department</a></li>
                    </ul> -->
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section department-single">
    <div class="container">

        <div class="row">
            <div class="col-lg-8">
                <div class="department-content mt-5">
                    <a href="<?=Url::to(Yii::$app->params['custom_url'] . 'education')?>" class="read-more"><i class="icofont-simple-left mr-2"></i> Back</a>

                    <h3 class="text-md">Myopia - How Severe is The Problem</h3>
                    <div class="divider my-4"></div>

                    <p>Myopia or shortsightedness has slowly been acknowledged as an increasing global health epidemic; sweeping across the developed as well as the developing countries. Some have likened this to a ticking time bomb. The advent of the digital age and the increasing use of electronic gadgets have seen a sharp rise in the prevalence of myopia.</p>
                    <p>Currently there are more than 2 and half billion people diagnosed with myopia worldwide. This figure has been estimated to rise to five billion by the year 2050. The hot spots for myopia are seen predominantly in Asian countries whereby; the prevalence of myopia has been found to be almost 80% – 90% with high myopes (more than -5D) about 10%. There is also an increasing trend of myopia with younger age groups. </p>
                    <p>It has been acknowledged that myopia will become the leading cause of blindness worldwide in the future. This leads to a substantive economic burden estimated around USD202 billion per annum. Most concerning is that vision loss from myopia can hinder education with children 2-5 times less likely to be in a formal education.</p>
                    <p>The growing problem of myopia has been compounded by the recent Covid-19 pandemic. It has been shown that home restrictions as well as increased screen time from online schooling has increased the prevalence of myopia by 1.5 to 3 times. Hence, there is a need for a concerted effort to address this growing health issue.</p>

<!--                    <a href="appoinment.html" class="btn btn-main-2 btn-round-full">Make an Appoinment<i class="icofont-simple-right ml-2  "></i></a>-->
                </div>
            </div>

            <div class="col-lg-4">
                <div class="sidebar-widget schedule-widget mt-5">
                    <h5 class="mb-4">Authors</h5>

                    <strong>1) Dr Sangeetha Tharmaturai</strong>
                    <p class="ml-3">Paediatric Ophthalmologist </p>
                    <p class="ml-3">Hospital Klang</p>
                    <br>
                    <strong>2) Dr Fazilawati </strong>
                    <p class="ml-3">Paediatric Ophthalmologist</p>
                    <p class="ml-3">Hospital Klang</p>

                </div>
            </div>
        </div>
    </div>
</section>