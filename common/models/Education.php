<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "education".
 *
 * @property int $id
 * @property string $title
 * @property string|null $short_description
 * @property string|null $image
 * @property string|null $details
 * @property string|null $author
 * @property int|null $created_at
 * @property int|null $updated_at
 */
class Education extends \yii\db\ActiveRecord
{
    public $img;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'education';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['details','author'], 'string'],
            [['created_at', 'updated_at'], 'integer'],
            [['title', 'short_description', 'image'], 'string', 'max' => 250],
            [['img'], 'file','maxSize'=>20*1024*1024,'skipOnEmpty' => true],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'short_description' => Yii::t('app', 'Short Description'),
            'image' => Yii::t('app', 'Image'),
            'details' => Yii::t('app', 'Details'),
            'author' => Yii::t('app', 'Author'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return EducationQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new EducationQuery(get_called_class());
    }
}
