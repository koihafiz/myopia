<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "committee".
 *
 * @property int $id
 * @property string $name
 * @property string|null $role
 * @property string|null $image
 * @property string|null $description
 * @property int|null $created_at
 * @property int|null $updated_at
 */
class Committee extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $img;

    public static function tableName()
    {
        return 'committee';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['description'], 'string'],
            [['created_at', 'updated_at'], 'integer'],
            [['name', 'role', 'image'], 'string', 'max' => 250],
            [['img'], 'file','maxSize'=>20*1024*1024,'skipOnEmpty' => true],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'role' => Yii::t('app', 'Role'),
            'image' => Yii::t('app', 'Image'),
            'description' => Yii::t('app', 'Description'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return CommitteeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CommitteeQuery(get_called_class());
    }
}
