<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "healthcare".
 *
 * @property int $id
 * @property string|null $category
 * @property string|null $title
 * @property string|null $description
 * @property string|null $image
 * @property string|null $details
 * @property int|null $created_at
 * @property int|null $updated_at
 */
class Healthcare extends \yii\db\ActiveRecord
{
    public $img;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'healthcare';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['details', 'description'], 'string'],
            [['created_at', 'updated_at'], 'integer'],
            [['category', 'title'], 'required'],
            [['category', 'title', 'image'], 'string', 'max' => 250],
            [['img'], 'file','maxSize'=>20*1024*1024,'skipOnEmpty' => true],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'category' => Yii::t('app', 'Category'),
            'title' => Yii::t('app', 'Title'),
            'description' => Yii::t('app', 'Description'),
            'image' => Yii::t('app', 'Image'),
            'details' => Yii::t('app', 'Details'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return HealthcareQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new HealthcareQuery(get_called_class());
    }
}
