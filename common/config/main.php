<?php
return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'timeZone' => 'Asia/Kuala_Lumpur',
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'modules' => [
        'admin' => [
            'class' => 'common\modules\admin\Admin',
        ],
        'redactor' => [
            'class'=>'yii\redactor\RedactorModule',
            'uploadDir' => '@webroot/uploads/education', //'@webroot/review',
            //'uploadUrl' => 'http://hybrizy.dev/review',
            //'uploadUrl' => 'http://192.168.11.119/uploads/review',
            'uploadUrl' => 'http://myopia2.test/uploads/education',
//            'uploadUrl' => 'https://byte2c.com/dev/myopia/uploads/education',
            'imageAllowExtensions'=>['jpg','png','gif']
        ]
    ],
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
    ],
];
